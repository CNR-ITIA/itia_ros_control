# Extension of ROS-CONTROL developed by CNR-ITIA (www.itia.cnr.it)

The repository contains the implementation of hardware interfaces and controller manager developed by the Institute of Industrial Technologies and Automation, of the National Research Council of Italy (CNR-ITIA).


## itia_configuration_manager [see package readme](itia_configuration_manager/README.md)

Configuration (aka metacontroller) manager developed by CNR-ITIA (www.itia.cnr.it)

The repository contains the implementation of a controller manager which load and unload multiple hardware interfaces and the related controllers.



## itia_basic_hardware_interface [see package readme](itia_basic_hardware_interface/README.md)
Basic hardware interface manager that can be used in the configurations (aka metacontroller) managed by the itia_controller_manager. 

The repository contains the implementation of a hardware interface with some additional basic functionalities wrt to the ROS control hardware interface. Moreover, a nodelet implementation is provided.



## itia_topic_hardware_interface [see package readme](itia_topic_hardware_interface/README.md)
The repository contains the implementation of a hardware interface derived from **itia_basic_hardware_interface**.


## controller_manager_example

Example of usage ___TODO___

## scripts

A collaction of Matlab scripts useful for the ITIA_ROS_CONTROL

## Developer Contact

**Authors:**   
- Manuel Beschi (manuel.beschi@itia.cnr.it)  
- Nicola Pedrocchi (nicola.pedrocchi@itia.cnr.it)  
- Stefano Ghidini (stefano.ghidini@itia.cnr.it)  

 
_Software License Agreement (BSD License)_    
_Copyright (c) 2017, National Research Council of Italy, Institute of Industrial Technologies and Automation_    
_All rights reserved._