cmake_minimum_required(VERSION 2.8.3)
project(itia_basic_hardware_interface)
add_compile_options(-std=c++11 -funroll-loops -Wall -Ofast)

find_package(catkin REQUIRED COMPONENTS
  controller_interface
  controller_manager
  diagnostic_msgs
  hardware_interface
  nodelet
  roscpp
  sensor_msgs
  configuration_msgs
)

catkin_package(
 INCLUDE_DIRS include
 LIBRARIES itia_basic_hardware_interface
 CATKIN_DEPENDS controller_interface controller_manager diagnostic_msgs hardware_interface  nodelet roscpp sensor_msgs configuration_msgs
 DEPENDS 
)

include_directories(
include
  ${catkin_INCLUDE_DIRS}
)
add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}/itia_basic_hardware_interface.cpp
  src/${PROJECT_NAME}/basic_hi_nodelet.cpp
)

add_dependencies(itia_basic_hardware_interface ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(itia_basic_hardware_interface
  ${catkin_LIBRARIES}
)

