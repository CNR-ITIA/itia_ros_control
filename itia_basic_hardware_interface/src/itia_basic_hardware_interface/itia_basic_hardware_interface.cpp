#include <itia_basic_hardware_interface/itia_basic_hardware_interface.h>


namespace itia_hardware_interface
{

BasicRobotHW::BasicRobotHW()
{
  m_status = created;
}

bool BasicRobotHW::init(ros::NodeHandle& root_nh, ros::NodeHandle& robot_hw_nh)
{
  ROS_WARN(">>>> New RobotHW initialization (%s)", robot_hw_nh.getNamespace().c_str() );
  m_robot_hw_nh = robot_hw_nh;
  m_robot_hw_nh.setCallbackQueue(&m_queue);
  m_root_nh     = root_nh;
  
  m_stop_thread    = false;
  m_service_thread = std::thread(&itia_hardware_interface::BasicRobotHW::serviceThread, this);
  m_status         = initialized;
  ROS_WARN("<<<< New RobotHW initialized (%s)", robot_hw_nh.getNamespace().c_str() );
  return true;
}

void BasicRobotHW::serviceThread()
{
  ros::ServiceServer m_get_params_server;
  ros::ServiceServer m_set_params_server;
  
  m_set_params_server=m_robot_hw_nh.advertiseService("writeParams",&itia_hardware_interface::BasicRobotHW::setParamServer,this);
  m_get_params_server=m_robot_hw_nh.advertiseService("readParams",&itia_hardware_interface::BasicRobotHW::getParamServer,this);
  while(true)
  {
    if (m_stop_thread)
      break;
    m_queue.callAvailable();
    ros::WallDuration(0.01).sleep();
  }
}



bool BasicRobotHW::checkForConflict(const std::__cxx11::list< hardware_interface::ControllerInfo >& info) const
{
  return hardware_interface::RobotHW::checkForConflict(info);
  
}

bool BasicRobotHW::prepareSwitch(const std::__cxx11::list< hardware_interface::ControllerInfo >& start_list, const std::__cxx11::list< hardware_interface::ControllerInfo >& stop_list)
{
  return true;
}

void BasicRobotHW::doSwitch(const std::__cxx11::list< hardware_interface::ControllerInfo >& start_list, const std::__cxx11::list< hardware_interface::ControllerInfo >& stop_list)
{
  return ;
}

diagnostic_msgs::DiagnosticArray BasicRobotHW::checkErrors()
{
  diagnostic_msgs::DiagnosticArray msg=m_diagnostic;
  m_diagnostic.status.resize(0);
  msg.header.stamp=ros::Time::now();
  return msg;
}

bool BasicRobotHW::getParamServer(configuration_msgs::GetConfigRequest& req, configuration_msgs::GetConfigResponse& res)
{
  return true;
}
bool BasicRobotHW::setParamServer(configuration_msgs::SetConfigRequest& req, configuration_msgs::SetConfigResponse& res)
{
  return true;
}


}
