#include <itia_basic_hardware_interface/basic_hi_nodelet.h>
#include <pluginlib/class_list_macros.h>
# include <diagnostic_msgs/DiagnosticArray.h>

size_t line = __LINE__;
#define __LL__ line = __LINE__;

PLUGINLIB_EXPORT_CLASS(itia::control::BasicHwIfaceNodelet, nodelet::Nodelet) 

#define FATAL_THROW( MSG )\
      ROS_FATAL("ERROR DURING STARTING HARDWARE INTERFACE '%s': %s", getPrivateNodeHandle().getNamespace().c_str(), std::string( MSG ).c_str() );\
      throw std::runtime_error( ("ERROR DURING STARTING HARDWARE INTERFACE '" + getPrivateNodeHandle().getNamespace() +"': " + std::string( MSG ) ).c_str()  )

namespace itia
{
namespace control
{
  
BasicHwIfaceNodelet::~BasicHwIfaceNodelet()
{
  ROS_INFO("[%s] shutting down...", m_console_name.c_str());
  m_stop = true;
  if (m_main_thread.joinable())
  {
    m_main_thread.join();
  }
  if (m_update_thread.joinable())
  {
    m_update_thread.join();
  }
  ROS_INFO("[%s] shutted down", m_console_name.c_str());
  m_stop = false;
}

void BasicHwIfaceNodelet::onInit()
{
  m_hw.reset( new itia_hardware_interface::BasicRobotHW() );
  
  startThreads();
}

void BasicHwIfaceNodelet::mainThread()
{
  m_console_name = getPrivateNodeHandle().getNamespace()+" type: BasicHwIfaceNodelet";
  ROS_DEBUG("[%s] STARTING", m_console_name.c_str());
  m_stop = false;
  
  try
  {
__LL__;    m_diagnostics_pub = getPrivateNodeHandle().advertise<diagnostic_msgs::DiagnosticArray>("/diagnostics",1);
__LL__;    double sampling_period=0.001;
           if (!getPrivateNodeHandle().getParam("sampling_period", sampling_period))
           {
__LL__;      ROS_WARN_STREAM(getPrivateNodeHandle().getNamespace()+"/sampling_period' does not exist, set equal to 0.001");
__LL__;      sampling_period = 1.0e-3;
           }
__LL__;    m_period=ros::Duration(sampling_period);
__LL__;
__LL__;    double diagnostic_period;
           if (!getPrivateNodeHandle().getParam("diagnostic_period", diagnostic_period))
           {
__LL__;      ROS_DEBUG_STREAM(getPrivateNodeHandle().getNamespace()+"/diagnostic_period' does not exist, set equal to 10xsampling_period");
__LL__;      diagnostic_period = 10*sampling_period;
           }
__LL__;    ros::Duration diagnostic_duration(diagnostic_period);
__LL__;
__LL__;
__LL__;    if (!m_hw->init(getNodeHandle(),getPrivateNodeHandle()))
           {
__LL__;      ROS_ERROR("FAIL DURING HARDWARE INTERFACE INITIALIZATION");
__LL__;      m_main_thread_state = ON_ERROR;
__LL__;      return;
           }
__LL__;
__LL__;    // getNodeHandle(), parent nodelet handle (typically "/")
__LL__;    // getPrivateNodeHandle() -> /nodelet_name/
__LL__;    m_cm.reset(new controller_manager::ControllerManager (m_hw.get(), getPrivateNodeHandle()));
__LL__;
__LL__;    m_update_thread_state = ON_INIT;
__LL__;    m_update_thread = std::thread(&itia::control::BasicHwIfaceNodelet::updateThread, this);
__LL__;    ros::Time start = ros::Time::now();
__LL__;    while( m_update_thread_state != RUNNING )
           {
__LL__;      if( m_update_thread_state == ON_ERROR )
             {
__LL__;        FATAL_THROW("The update thread is in Error. Abort.");
             }
__LL__;      if( ( ros::Time::now() - start ).toSec() > 10.0 )
             {
__LL__;        FATAL_THROW("Timeout Expired. The update thread did not yet started. Abort.");
             }
__LL__;      ros::Duration(0.05).sleep();
           }
__LL__;
__LL__;    ROS_DEBUG("STARTED HARDWARE INTERFACE '%s'", getPrivateNodeHandle().getNamespace().c_str());
__LL__;    ROS_DEBUG("Frequency: '%f'", 1.0/sampling_period);
__LL__;    ROS_DEBUG("Controller joints:");
__LL__;    for (auto& elem: m_hw->getNames())
           {
__LL__;      ROS_DEBUG("- '%s'", elem.c_str());
           }
__LL__;
__LL__;    m_main_thread_state = RUNNING;
__LL__;    while (ros::ok() && !m_stop)
           {
__LL__;      diagnostic_msgs::DiagnosticArray diag=m_hw->checkErrors();
__LL__;      if (diag.status.size()>0)
             {
               m_diagnostics_pub.publish(diag);
              }

__LL__;      if (m_hw->getStatus()==itia_hardware_interface::with_error)
             {
__LL__;        ROS_FATAL("Hardware interface is in error state");
__LL__;        m_main_thread_state = ON_ERROR;
             }
__LL__;      diagnostic_duration.sleep();
           }
  }
  catch(std::exception& e)
  {
    m_stop = true;
    ROS_FATAL("mainThread error %s (at line:%zu)",e.what(), line);
    m_main_thread_state = ON_ERROR;
  }
  
  if (m_hw->getStatus()==itia_hardware_interface::with_error)
  {
    ROS_FATAL("Hardware interface is in error state, shutting down"); 
  }

  if (!m_update_thread.joinable())
  {
    m_stop = true;
    m_update_thread.join();
  }
  m_hw.reset();
  m_main_thread_state = EXPIRED;
  return;  
  
}

void BasicHwIfaceNodelet::updateThread()
{
  ros::WallRate wr(m_period);
  bool is_ok=true;
  unsigned int number_of_error=0;
  m_update_thread_state = RUNNING;
  while (ros::ok() && !m_stop && (number_of_error<=3))
  {
    try
    {
      m_hw->read(ros::Time::now(),m_period);
    }
    catch(std::exception& e)
    {
      ROS_FATAL("updateThread error call hardware interface %s read() %s",m_console_name.c_str(),e.what());
      is_ok=false;
    }
    try
    {
      m_cm->update(ros::Time::now(), m_period);
    }
    catch(std::exception& e)
    {
      ROS_FATAL("updateThread error call controller manager %s update() %s",m_console_name.c_str(),e.what());
      is_ok=false;
      m_stop=true;
      is_ok=false;
      m_update_thread_state = ON_ERROR;
      return;
    }
    try
    {
      m_hw->write(ros::Time::now(),m_period);
      wr.sleep();
    }
    catch(std::exception& e)
    {
      ROS_FATAL("updateThread error call hardware interface %s write() %s",m_console_name.c_str(),e.what());
      is_ok=false;
    }
    
    if (m_hw->getStatus()==itia_hardware_interface::with_error)
      number_of_error++;
    
    if (!is_ok)
      number_of_error++;
    
  }
  m_stop = true;
  m_update_thread_state = EXPIRED;
}

void BasicHwIfaceNodelet::startThreads()
{
  m_main_thread_state = ON_INIT;
  m_main_thread       = std::thread(&itia::control::BasicHwIfaceNodelet::mainThread, this);
  
  ros::Time start     = ros::Time::now();
  while( m_main_thread_state != RUNNING )
  {
    if( m_main_thread_state == ON_ERROR )
    {
      FATAL_THROW( "Main thread in ERROR" );
    }
    if( ( ros::Time::now() - start ).toSec() > 10.0 )
    {
      FATAL_THROW( "Timeout Exipred. Main Thread did not started yet. Abort." );
    }
    ros::Duration(0.05).sleep();
  }

  ROS_DEBUG("[%s] STARTED", m_console_name.c_str());
}


 
}
}

#undef FATAL_THROW
