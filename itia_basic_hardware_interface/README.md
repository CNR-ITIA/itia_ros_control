# Basic hardware interface manager that can be used in the configurations (aka metacontroller) managed by the itia_controller_manager. It is developed by CNR-ITIA (www.itia.cnr.it)

The repository contains the implementation of a hardware interface with some additional basic functionalities wrt to the ROS control hardware interface. Moreover, a nodelet implementation is provided.
The package is developed by the Institute of Industrial Technologies and Automation, of the National Research Council of Italy (CNR-ITIA).


# Functionalities and code organization

The package **itia_basic_hardware_interface** is made the following classes: 

1) **BasicRobotHW** implements an hardware interface with some additional features like:
> _checkErrors_ with provides a diagnostic array messages with useful information on error/problem/warnings to be published on _/diagnostics_ topic,

> _setParamServer_ allows setting system parameters by using _itia_msgs/set_config_ service,

> _getParamServer_ allows reading system parameters by using _itia_msgs/get_config_ service.


2) The hardware interface is instantiated on **itia::control::BasicHwIfaceNodelet**, a nodelet that can be (un)loaded by **itia_controller_manager**. This nodelet has two threads:

> 1) _mainThread()_ publishes information on _/diagnostics_  topic every _diagnostic_period_. It instatiantes the ROS-Control controller_manager

> 2) _updateThread()_  updates the hardware interface every _sampling_period_.

***NOTE:*** Children nodelets has only to redefine the _onInit()_ and _shutdown()_ methods. The _onInit()_ method will be load a child of **itia::control::BasicHwIfaceNodelet** while, _shutdown()_ method can be use to reset shared_ptr is needed. 

It is worth stressing that only the **itia::control::BasicHwIfaceNodelet** destructor _~BasicRobotHW()_ will be called during the nodelet unloading. _~BasicRobotHW()_ calls the _shutdown()_ of the child. 

## Required parameters

Basic hardware interface requires the following parameters:

```yaml
hardware_interface_name:
  type: "itia/control/BasicHwIfaceNodelet"
  joint_names: # List of controller joint names
  - "joint_1"
  - "joint_2"
  - "joint_3"
  allowed_controller_types: # List of plugable controllers. TO BE DONE
  - "controller_type1"  
  sampling_period: 0.1 # read/write period
  diagnostic_period: 0.1 # diagnostic period
  
  # optional parameters
  remap_source_args: ['topic1','topic2']  #old names
  remap_target_args: ['new_topic1','new_topic2'] #new names
```

## Developer Contact

**Authors:**   
- Manuel Beschi (manuel.beschi@itia.cnr.it)  
- Nicola Pedrocchi (nicola.pedrocchi@itia.cnr.it)  
 
_Software License Agreement (BSD License)_    
_Copyright (c) 2017, National Research Council of Italy, Institute of Industrial Technologies and Automation_    
_All rights reserved._