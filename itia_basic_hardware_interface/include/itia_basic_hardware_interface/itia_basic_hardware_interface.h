#ifndef __ITIA_BASIC_HARDWARE_INTERFACE__
#define __ITIA_BASIC_HARDWARE_INTERFACE__

# include <hardware_interface/joint_command_interface.h>
# include <hardware_interface/robot_hw.h>
# include <ros/ros.h>
# include <diagnostic_msgs/DiagnosticArray.h>
# include <configuration_msgs/SetConfig.h>
# include <configuration_msgs/GetConfig.h>
# include <ros/callback_queue.h>
# include <thread>

namespace itia_hardware_interface
{
  enum status { created, initialized, run, with_error };
    class BasicRobotHW: public hardware_interface::RobotHW
    {
    public:
      BasicRobotHW();
      virtual ~BasicRobotHW()
      {
        m_stop_thread=true;
        shutdown();
        if (m_service_thread.joinable())
          m_service_thread.join();
        
      };
      virtual void shutdown(){}; //useful to delete pointer
      virtual void read(const ros::Time& time, const ros::Duration& period){};
      virtual void write(const ros::Time& time, const ros::Duration& period){};
      
      virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle &robot_hw_nh);
      
      virtual bool checkForConflict(const std::list<hardware_interface::ControllerInfo>& info) const;
      virtual bool prepareSwitch(const std::list<hardware_interface::ControllerInfo>& start_list,
                                 const std::list<hardware_interface::ControllerInfo>& stop_list);
                                 
      virtual void doSwitch(const std::list<hardware_interface::ControllerInfo>& start_list,
                            const std::list<hardware_interface::ControllerInfo>& stop_list);

     
      diagnostic_msgs::DiagnosticArray checkErrors();
      itia_hardware_interface::status getStatus(){return m_status;};
                                                      
    protected:
      
      virtual bool setParamServer(configuration_msgs::SetConfigRequest& req, configuration_msgs::SetConfigResponse& res);
      virtual bool getParamServer(configuration_msgs::GetConfigRequest& req, configuration_msgs::GetConfigResponse& res);
      
      /* serviceThread manages the service requests without interrupt the main loop
       * 
       */
      virtual void serviceThread();
      
      ros::NodeHandle m_root_nh;
      ros::NodeHandle m_robot_hw_nh;
      
      ros::CallbackQueue m_queue;
      
      diagnostic_msgs::DiagnosticArray m_diagnostic;
      std::thread m_service_thread;
      bool m_stop_thread;
      
      status m_status;
      
    };
}

#endif
