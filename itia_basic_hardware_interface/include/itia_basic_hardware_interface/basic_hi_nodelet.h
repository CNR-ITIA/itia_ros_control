#ifndef __BASIC_HARDWARE_INTERFACE__ON_NODELET__
#define __BASIC_HARDWARE_INTERFACE__ON_NODELET__

# include <controller_manager/controller_manager.h>
# include <nodelet/nodelet.h>
# include <thread>
# include <itia_basic_hardware_interface/itia_basic_hardware_interface.h>

namespace itia
{
  
  namespace control
  {
    
    class BasicHwIfaceNodelet : public nodelet::Nodelet
    {
    public:
      virtual void onInit();
      
    protected:
      
      enum THREAD_STATE { ON_INIT, RUNNING, ON_ERROR, EXPIRED } m_main_thread_state, m_update_thread_state;
      
      std::thread m_main_thread;
      std::thread m_update_thread;
      
      bool        m_stop;
      
      std::shared_ptr<itia_hardware_interface::BasicRobotHW> m_hw;
      std::shared_ptr<controller_manager::ControllerManager> m_cm;
      ros::Publisher  m_diagnostics_pub;
      ros::Duration   m_period;
      
      void startThreads();
      void mainThread();
      void updateThread();
      ~BasicHwIfaceNodelet();
      
      std::string m_console_name;
    };
    
    
    
  }
}
# endif
