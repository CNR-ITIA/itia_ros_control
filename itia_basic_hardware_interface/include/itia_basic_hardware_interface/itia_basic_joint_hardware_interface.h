#ifndef __ITIA_BASIC_JOINT_HARDWARE_INTERFACE__
#define __ITIA_BASIC_JOINT_HARDWARE_INTERFACE__

# include <itia_basic_hardware_interface/itia_basic_hardware_interface.h>

namespace itia_hardware_interface
{
    class BasicJointRobotHW: public BasicRobotHW
    {
    public:
      BasicJointRobotHW();
      virtual ~BasicJointRobotHW()
      {
        ~itia_hardware_interface::BasicRobotHW();
      };
      
      virtual void shutdown(){}; //useful to delete pointer
      virtual void read(const ros::Time& time, const ros::Duration& period){};
      virtual void write(const ros::Time& time, const ros::Duration& period){};
      
      virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle &robot_hw_nh) ;
      
      virtual bool checkForConflict(const std::list<hardware_interface::ControllerInfo>& info) const;
      virtual bool prepareSwitch(const std::list<hardware_interface::ControllerInfo>& start_list,
                                 const std::list<hardware_interface::ControllerInfo>& stop_list);
                                 
      virtual void doSwitch(const std::list<hardware_interface::ControllerInfo>& start_list,
                            const std::list<hardware_interface::ControllerInfo>& stop_list);

     
      diagnostic_msgs::DiagnosticArray checkErrors();
      
                                                      
    protected:
      virtual bool setParamServer(itia_msgs::set_configRequest& req, itia_msgs::set_configResponse& res);
      virtual bool getParamServer(itia_msgs::get_configRequest& req, itia_msgs::get_configResponse& res);
      
      /* serviceThread manages the service requests without interrupt the main loop
       * 
       */
      virtual void serviceThread();
      
      ros::NodeHandle m_root_nh;
      ros::NodeHandle m_robot_hw_nh;
      
      ros::CallbackQueue m_queue;
      
      diagnostic_msgs::DiagnosticArray m_diagnostic;
      std::thread m_service_thread;
      bool m_stop_thread;
      
      status m_status;
      
    };
}

#endif
