#include <itia_fake_hardware_interface/itia_fake_hardware_interface.h>



namespace itia_hardware_interface
{
  
FakeRobotHW::FakeRobotHW(std::vector< std::string > joint_names)
{
  m_joint_names=joint_names;
  m_nAx=m_joint_names.size();
  
  if (m_nAx==0)
    ROS_WARN("[%s] no joints selected",m_robot_hw_nh.getNamespace().c_str());
  
}

bool FakeRobotHW::init(ros::NodeHandle& root_nh, ros::NodeHandle& robot_hw_nh)
{
  if (!itia_hardware_interface::BasicRobotHW::init(root_nh, robot_hw_nh))
  {
    ROS_ERROR("[%s] BasicRobotHW error",robot_hw_nh.getNamespace().c_str());
    return false;
  }
  
  m_pos.resize(m_nAx);
  m_vel.resize(m_nAx);
  m_eff.resize(m_nAx);
  
  std::fill(m_pos.begin(),m_pos.end(),0.0);
  std::fill(m_vel.begin(),m_vel.end(),0.0);
  std::fill(m_eff.begin(),m_eff.end(),0.0);
  
  if (m_robot_hw_nh.hasParam("initial_position"))
    m_robot_hw_nh.getParam("initial_position",m_pos);
  
  double timeout=10;
  if (!m_robot_hw_nh.getParam("feedback_joint_state_timeout",timeout))
  {
    ROS_INFO("[%s] feedback_joint_state_timeout not defined, set equal to 10",robot_hw_nh.getNamespace().c_str());
    timeout=10;
  }
  
  m_cmd_pos.resize(m_nAx);
  m_cmd_vel.resize(m_nAx);
  m_cmd_eff.resize(m_nAx);
  
  m_cmd_pos=m_pos;
  m_cmd_vel=m_vel;
  m_cmd_pos=m_eff;
  
  for (std::string& joint_name: m_joint_names) 
  {
    
    auto i = &joint_name-&m_joint_names[0];
    
    hardware_interface::JointStateHandle state_handle(joint_name, 
                                                      &(m_pos.at(i)), 
                                                      &(m_vel.at(i)), 
                                                      &(m_eff.at(i)));
    
    
    m_js_jh.registerHandle(state_handle);
    
    m_p_jh.registerHandle( hardware_interface::JointHandle(state_handle, &(m_cmd_pos.at(i))) );
    m_v_jh.registerHandle( hardware_interface::JointHandle(state_handle, &(m_cmd_vel.at(i))) );
    m_e_jh.registerHandle( hardware_interface::JointHandle(state_handle, &(m_cmd_eff.at(i))) );
    
    m_pve_jh.registerHandle(hardware_interface::PosVelEffJointHandle(state_handle,&(m_cmd_pos.at(i)),&(m_cmd_vel.at(i)),&(m_cmd_eff.at(i))));
    m_ve_jh.registerHandle(hardware_interface::VelEffJointHandle(state_handle,&(m_cmd_vel.at(i)),&(m_cmd_eff.at(i))));
  }
  
  
  
  registerInterface(&m_js_jh);
  registerInterface(&m_p_jh);
  registerInterface(&m_v_jh);
  registerInterface(&m_e_jh);
  registerInterface(&m_pve_jh);
  registerInterface(&m_ve_jh);
  
  m_p_jh_active=m_v_jh_active=m_e_jh_active=false;
  return true;
}

void FakeRobotHW::read(const ros::Time& time, const ros::Duration& period)
{
}

void FakeRobotHW::write(const ros::Time& time, const ros::Duration& period)
{
  
  if (!m_p_jh_active && !m_v_jh_active && !m_e_jh_active)
  {
    ROS_DEBUG_THROTTLE(2,"no active controller");
    return;
  }
  
  
  if (m_p_jh_active)
    m_pos = m_cmd_pos;
  else
  {
    std::fill(m_pos.begin(),m_pos.end(),0.0);
  }
  
  if (m_v_jh_active)
    m_vel = m_cmd_vel;
  else
  {
    m_vel.resize(m_nAx);
    std::fill(m_vel.begin(),m_vel.end(),0.0);
  }
  
  if (m_e_jh_active)
    m_eff   = m_cmd_eff;
  else
  {
    m_eff.resize(m_nAx);
    std::fill(m_eff.begin(),m_eff.end(),0.0);
  }
}

bool FakeRobotHW::prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
  bool p_jh_active, v_jh_active, e_jh_active;

  p_jh_active=m_p_jh_active;
  v_jh_active=m_v_jh_active;
  e_jh_active=m_e_jh_active;

  for (const hardware_interface::ControllerInfo& controller: stop_list)
  {
    for (const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      if (!res.hardware_interface.compare("hardware_interface::PositionJointInterface"))
        p_jh_active=false;
      if (!res.hardware_interface.compare("hardware_interface::VelocityJointInterface"))
        v_jh_active=false;
      if (!res.hardware_interface.compare("hardware_interface::EffortJointInterface"))
        e_jh_active=false;
      if (!res.hardware_interface.compare("hardware_interface::VelEffJointInterface"))
      {
        v_jh_active=false;
        e_jh_active=false;
      }
      if (!res.hardware_interface.compare("hardware_interface::PosVelEffJointInterface"))
      {
        p_jh_active=false;
        v_jh_active=false;
        e_jh_active=false;
      }
    }

  }

  for (const hardware_interface::ControllerInfo& controller: start_list)
  {
    
    for (const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      if (!res.hardware_interface.compare("hardware_interface::PositionJointInterface"))
        p_jh_active=true;
      if (!res.hardware_interface.compare("hardware_interface::VelocityJointInterface"))
        v_jh_active=true;
      if (!res.hardware_interface.compare("hardware_interface::EffortJointInterface"))
        e_jh_active=true;
      if (!res.hardware_interface.compare("hardware_interface::VelEffJointInterface"))
      {
        v_jh_active=true;
        e_jh_active=true;
      }
      if (!res.hardware_interface.compare("hardware_interface::PosVelEffJointInterface"))
      {
        p_jh_active=true;
        v_jh_active=true;
        e_jh_active=true;
      }
    }
  }
  m_p_jh_active=p_jh_active;
  m_v_jh_active=v_jh_active;
  m_e_jh_active=e_jh_active;
  return true;
}

void FakeRobotHW::doSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
}

bool FakeRobotHW::checkForConflict(const std::list< hardware_interface::ControllerInfo >& info) 
{
  // Each controller can use more than a hardware_interface for a single joint (for example: position, velocity, effort). 
  // One controller can control more than one joint.
  // A joint can be used only by a controller.
  
  std::vector<bool> global_joint_used(m_nAx); 
  std::fill(global_joint_used.begin(),global_joint_used.end(),false);
  
  for (hardware_interface::ControllerInfo controller: info)
  {
    std::vector<bool> single_controller_joint_used(m_nAx); 
    std::fill(single_controller_joint_used.begin(),single_controller_joint_used.end(),false);
    
    for (hardware_interface::InterfaceResources res: controller.claimed_resources)
    {
      for (std::string name: res.resources)
      {
        for (unsigned int iJ=0;iJ<m_nAx;iJ++)
        {
          if (!name.compare(m_joint_names.at(iJ)))
          {
            if (global_joint_used.at(iJ)) // if already used by another
            {
              ROS_ERROR("Joint %s is already used by another controller",name.c_str());
              diagnostic_msgs::DiagnosticStatus diag;
              diag.name=m_robot_hw_nh.getNamespace();
              diag.hardware_id=m_robot_hw_nh.getNamespace();
              diag.level=diagnostic_msgs::DiagnosticStatus::ERROR;
              diag.message="Hardware interface "+m_robot_hw_nh.getNamespace()+" run time: Joint " + name + " is already used by another controller";
              m_diagnostic.status.push_back(diag);
              
              return true;
            }
            else
              single_controller_joint_used.at(iJ);
          }
        }
      }
    }
    for (unsigned int iJ=0;iJ<m_nAx;iJ++)
      global_joint_used.at(iJ)= global_joint_used.at(iJ) || single_controller_joint_used.at(iJ);
    
  }
  return false;
}

void FakeRobotHW::shutdown()
{
  ROS_INFO("shutdown fake");
}


}

