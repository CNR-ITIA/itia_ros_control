#ifndef __ITIA_FAKE_HARDWARE_INTERFACE__
#define __ITIA_FAKE_HARDWARE_INTERFACE__

#include <itia_basic_hardware_interface/itia_basic_hardware_interface.h>
#include <hardware_interface/posvelacc_command_interface.h>
#include <itia_basic_hardware_interface/posveleff_command_interface.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseStamped.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <mutex>




namespace itia_hardware_interface
{
  
  class FakeRobotHW: public itia_hardware_interface::BasicRobotHW
  {
  public:
    FakeRobotHW(std::vector<std::string> joint_names);
    virtual ~FakeRobotHW(){};
    virtual void shutdown();
    virtual void read(const ros::Time& time, const ros::Duration& period);
    virtual void write(const ros::Time& time, const ros::Duration& period);
    
    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle &robot_hw_nh) ;
    
    virtual bool checkForConflict(const std::list<hardware_interface::ControllerInfo>& info) ;
    
    virtual bool prepareSwitch(const std::list<hardware_interface::ControllerInfo>& start_list,
                               const std::list<hardware_interface::ControllerInfo>& stop_list);
    
    virtual void doSwitch(const std::list<hardware_interface::ControllerInfo>& start_list,
                          const std::list<hardware_interface::ControllerInfo>& stop_list);
    
    

    
  protected:
    hardware_interface::JointStateInterface    m_js_jh; //interface for reading joint state
    hardware_interface::PositionJointInterface m_p_jh; //interface for writing position target
    hardware_interface::VelocityJointInterface m_v_jh; //interface for writing velocity target
    hardware_interface::EffortJointInterface   m_e_jh; //interface for writing effort target
    hardware_interface::PosVelEffJointInterface m_pve_jh;
    hardware_interface::VelEffJointInterface m_ve_jh;
    
    
    bool m_p_jh_active;
    bool m_v_jh_active;
    bool m_e_jh_active;
    
    std::vector<std::string> m_joint_names;
    
    
    std::vector<double> m_pos; // feedback position
    std::vector<double> m_vel; // feedback velocity
    std::vector<double> m_eff; // feedback effort
    
    std::vector<double> m_cmd_pos; //target position
    std::vector<double> m_cmd_vel; //target velocity
    std::vector<double> m_cmd_eff; //target effort
    
    
    
    unsigned int m_nAx;
    
    std::mutex m_mutex;
    
    enum status { created, initialized, run, error };
    
  };
}

#endif
