#ifndef __FAKE_HARDWARE_INTERFACE__ON_NODELET__
#define __FAKE_HARDWARE_INTERFACE__ON_NODELET__

# include <controller_manager/controller_manager.h>
# include <nodelet/nodelet.h>
# include <thread>
# include <itia_fake_hardware_interface/itia_fake_hardware_interface.h>
# include <itia_basic_hardware_interface/basic_hi_nodelet.h>

namespace itia
{
  namespace control
  {
    
    class FakeHwIfaceNodelet : public BasicHwIfaceNodelet
    {
    public:
      virtual void onInit();
      
    protected:
    };
    
    
    
  }
}
# endif