cmake_minimum_required(VERSION 2.8.3)
project(itia_fake_hardware_interface)
add_compile_options(-std=c++11 -funroll-loops -Wall -Ofast)
set(CMAKE_BUILD_TYPE Debug)

find_package(catkin REQUIRED COMPONENTS
  itia_basic_hardware_interface
  nodelet
  roscpp
)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES itia_fake_hardware_interface
  CATKIN_DEPENDS itia_basic_hardware_interface  nodelet roscpp
  DEPENDS 
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}/itia_fake_hardware_interface.cpp
  src/${PROJECT_NAME}/fake_hi_nodelet.cpp
)
add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)
