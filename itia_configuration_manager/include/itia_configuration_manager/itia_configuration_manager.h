#ifndef HOTPLUG_CONFIGURATION_MANAGER__
#define HOTPLUG_CONFIGURATION_MANAGER__

#include <ros/ros.h>
#include <controller_manager_msgs/LoadController.h>
#include <controller_manager_msgs/UnloadController.h>
#include <controller_manager_msgs/SwitchController.h>
#include <controller_manager_msgs/ListControllers.h>
#include <nodelet/NodeletLoad.h>
#include <nodelet/NodeletUnload.h>
#include <nodelet/NodeletList.h>
#include <std_srvs/Trigger.h>
#include <boost/graph/graph_concepts.hpp>
#include <actionlib/client/simple_action_client.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <bondcpp/bond.h>
#include <mutex>
#include <configuration_msgs/ListConfigurations.h>
#include <configuration_msgs/StartConfiguration.h>
#include <configuration_msgs/StopConfiguration.h>
namespace itia_configuration_manager
{

class ConfigurationManager
{
private:
  ros::NodeHandle m_nh;
  
  std::string m_active_configuration_name;
  std::multimap<std::string,std::string> m_active_configuration;
  ros::ServiceClient m_load_nodelet;
  ros::ServiceClient m_unload_nodelet;
  ros::ServiceClient m_list_nodelet;
  XmlRpc::XmlRpcValue m_config;
  std::map<std::string,std::shared_ptr<bond::Bond>> m_bonds;
  bool m_use_bond;

  bool m_is_ok;
  
  bool get_hardware_interface_load_info(const std::string& name, nodelet::NodeletLoadRequest& request);
public:
  
  //! Controller manager provide service to start/stop configurations (aka metacontrollers)
  /*!
   *  \param nh NodeHandle of the controller manager
   */ 
  
  ConfigurationManager( const ros::NodeHandle& nh );
  ~ConfigurationManager();
  
  bool startCallback ( configuration_msgs::StartConfiguration::Request& req, configuration_msgs::StartConfiguration::Response& res);
  bool stopCallback ( configuration_msgs::StopConfiguration::Request& req, configuration_msgs::StopConfiguration::Response& res);
  bool listConfigurations(configuration_msgs::ListConfigurations::Request& req, configuration_msgs::ListConfigurations::Response& res);
  
  bool checkBondStatus();
  bool purgeBond();
  
  bool isOk(){return m_is_ok;};
  

};
}


#endif
