#include "ros/ros.h"
#include <itia_configuration_manager/itia_configuration_manager.h>


int main(int argc, char **argv){
  ros::init(argc, argv, "itia_configuration_manager");
  ros::NodeHandle nh;
  
  
  if (nh.hasParam("verbose"))
  {
    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME,ros::console::levels::Debug);
  }

  itia_configuration_manager::ConfigurationManager cm(nh);
  
  ros::ServiceServer load_configuration      = nh.advertiseService("/configuration_manager/start_configuration",&itia_configuration_manager::ConfigurationManager::startCallback,  &cm);
  ros::ServiceServer unload_configuration      = nh.advertiseService("/configuration_manager/stop_configuration",&itia_configuration_manager::ConfigurationManager::stopCallback, &cm);
  ros::ServiceServer list_controller_service = nh.advertiseService("/configuration_manager/list_configurations",  &itia_configuration_manager::ConfigurationManager::listConfigurations, &cm);
  
  ros::Rate lp(100);

  unsigned int trials=0;
  unsigned int max_trials=0;

  while ( (ros::ok()) /*&& (cm.isOk())*/ )
  {
    ros::spinOnce();
    if (!cm.checkBondStatus())
    {
      ROS_WARN("Stop configuration_manager because a bond is broken");
      configuration_msgs::StopConfiguration srv;
      srv.request.strictness=1;
      if (!cm.stopCallback(srv.request,srv.response))
      {
        ROS_WARN("Unable to call stop service, trying purging bonds");
        if (cm.purgeBond())
          ROS_WARN("Some bonds remained broken");
        if (cm.stopCallback(srv.request,srv.response))
        {
          ROS_WARN("Unable to stop again, stop configuration_manager");
          continue;
        }
        continue;
      }
      if (!srv.response.ok)
      {
        ROS_WARN("Unable to stop, trying purging bonds");
        if (cm.purgeBond())
          ROS_WARN("Some bonds remained broken");
        if (cm.stopCallback(srv.request,srv.response))
        {
          ROS_WARN("Unable to stop again, stop configuration_manager");
          continue;
        }
        continue;
      }
      
      
    }
    if (!cm.isOk())
    {
      ROS_WARN(" [ %s ] ******** Raised and error in the ConfigurationManager.   ********** " , ros::this_node::getName().c_str() );
      ROS_WARN(" [ %s ] ******** Calling Emergency Stop of all the configuration ********** " , ros::this_node::getName().c_str() );
      configuration_msgs::StopConfiguration srv;
      srv.request.strictness=1;
      if (!cm.stopCallback(srv.request,srv.response))
      {
        ROS_WARN("[ %s ] Service Broken. Calling Emergency Stop Failure. Exit" , ros::this_node::getName().c_str() );
        break;
      }
      if (!srv.response.ok)
      {
        if (trials++<max_trials)
        {
          ROS_WARN("[ %s ] Calling Emergency Stop Failed ... try to continue, the behavior may be unexpected" , ros::this_node::getName().c_str() );
          continue;
        }
        else
        {
          ROS_WARN("[ %s ] Calling Emergency Stop Failed too many times... Exit" , ros::this_node::getName().c_str() );
          break;
        }
      }
      
    }
    
    lp.sleep();
  }
  return 0;
}
