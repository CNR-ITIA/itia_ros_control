#include <itia_configuration_manager/itia_configuration_manager.h>
#include <bondcpp/bond.h>
namespace itia_configuration_manager 
{
  
std::multimap< std::string, std::string > get_controllers( XmlRpc::XmlRpcValue& config, const std::string& configuration )
{
  std::multimap< std::string, std::string > ret;
  ////////////////////////////////////////////////////////////////////////
  if (config.getType() != XmlRpc::XmlRpcValue::TypeArray)
  {
    ROS_ERROR("The param is not a list of configurations" );
    ret.clear();
    return ret;
  }
  
  std::vector<std::string> conf_names;
  ROS_DEBUG_STREAM("Scan of feasible configuration list (number of configurations:" << config.size() << ")"  );
  for(size_t i=0; i < config.size(); i++) 
  {
    XmlRpc::XmlRpcValue controller = config[i];  
    if( controller.getType() != XmlRpc::XmlRpcValue::TypeStruct)
    {
      ROS_WARN("The element #%zu is not a struct", i);
      continue;
    }
    if( !controller.hasMember("name") )
    {
      ROS_WARN("The element #%zu has not the field 'name'", i);
      continue;
    }
    if( !controller.hasMember( "components" ) )
    {
      ROS_WARN("The element #%zu has not the field 'components'", i);
      continue;
    }
    if( controller["components"].getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_WARN("The element #%zu has 'components' bad-formed", i);
      continue;
    }
    if( (std::string)(controller["name"]) != configuration )
    {
      continue;
    }    

    //----------------------------------------------
    if( controller.hasMember( "depends" ) )
    {
      std::string dep = (controller["depends"]);
      std::multimap< std::string, std::string > dep_components = get_controllers( config, dep );
      for( auto & comp : dep_components )
      {
        ret.insert( comp );
      }
    }
    
    XmlRpc::XmlRpcValue components = controller["components"];
    for( size_t i=0; i<components.size();i++)
    {
      
      if( ( components[i].getType() == XmlRpc::XmlRpcValue::TypeStruct)
      &&  ( components[i].hasMember("hardware_interface") )
      &&  ( components[i].hasMember("controller") ) )
      {
        ret.insert(std::pair<std::string,std::string>((std::string)(components[i]["hardware_interface"]),(std::string)(components[i]["controller"])));
      }
    }
     
  }
  return ret;
}

bool ConfigurationManager::get_hardware_interface_load_info(const std::string& name,nodelet::NodeletLoadRequest& request)
{
  XmlRpc::XmlRpcValue hardware_interface;
  if (!m_nh.getParam(name, hardware_interface ) )
  {
    ROS_ERROR("Param '%s' does not exist", name.c_str() );
    return false;
  }
  if( !hardware_interface.hasMember( "type" ) )
  {
    ROS_WARN("The hardware_interface has not the field 'type'");
    return false;
  }
  
  
  request.name=name;
  request.type= (std::string)hardware_interface["type"];
  request.bond_id="hardware_interface_"+request.type+"_"+request.name;
  
  if( hardware_interface.hasMember( "remap_source_args" ) )
  {
    
    XmlRpc::XmlRpcValue remap_source_args=hardware_interface["remap_source_args"];
    
    if (remap_source_args.getType() != XmlRpc::XmlRpcValue::TypeArray)
    {
      ROS_ERROR("The remap_source_args is not a list of names" );
      return false;
    }
    request.remap_source_args.resize(remap_source_args.size());
    for(size_t i=0; i < remap_source_args.size(); i++)
      request.remap_source_args.at(i)=(std::string)remap_source_args[i];
  }
  
  if( hardware_interface.hasMember( "remap_target_args" ) )
  {
    
    XmlRpc::XmlRpcValue remap_target_args=hardware_interface["remap_target_args"];
    
    if (remap_target_args.getType() != XmlRpc::XmlRpcValue::TypeArray)
    {
      ROS_ERROR("The remap_target_args is not a list of names" );
      return false;
    }
    if (request.remap_source_args.size() != remap_target_args.size())
    {
      ROS_ERROR("remap_target_args is different w.r.t. remap_target_args");
      return false;
    }
    request.remap_target_args.resize(remap_target_args.size());
    for(size_t i=0; i < remap_target_args.size(); i++)
      request.remap_target_args.at(i)=(std::string)remap_target_args[i];
  }
  return true;
  
}

ConfigurationManager::ConfigurationManager (const ros::NodeHandle& nh ) 
{
  if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {

    ros::console::notifyLoggerLevelsChanged();

  }

  m_active_configuration_name="None";
  m_nh=nh;
  m_load_nodelet    = m_nh.serviceClient<nodelet::NodeletLoad>  ("manager/load_nodelet");
  m_unload_nodelet  = m_nh.serviceClient<nodelet::NodeletUnload>("manager/unload_nodelet");
  m_list_nodelet    = m_nh.serviceClient<nodelet::NodeletList>  ("manager/list");
  
  m_load_nodelet.waitForExistence();
  
  
  m_active_configuration.clear();
  if (!m_nh.getParam("control_configurations", m_config ) )
  {    
    ROS_ERROR("[ Configuration Manger ] Param '%s/%s' does not exist", nh.getNamespace().c_str(), "control_configurations" );
    m_is_ok=false;
  }
  else 
    m_is_ok=true;
}


bool ConfigurationManager::startCallback ( configuration_msgs::StartConfiguration::Request& req, configuration_msgs::StartConfiguration::Response& res )
{
  
  controller_manager_msgs::LoadController   load_ctrl_srv;
  controller_manager_msgs::SwitchController switch_ctrl_srv;
  controller_manager_msgs::UnloadController unload_ctrl_srv;
  
  nodelet::NodeletLoad load_srv;
  
  
  res.ok=false;
  
  ROS_DEBUG("********* Starting controller: '%s'", req.start_configuration.c_str());

  ////////////////////////////////////////////////////////////////////////
  ROS_DEBUG("Active Controller: " );
  std::vector<std::string> active_hardware_interface;
  for(auto c : m_active_configuration )
  {
    active_hardware_interface.push_back(c.first);
    ROS_DEBUG("\t- %s, %s", c.first.c_str(),  c.second.c_str());
  }

  //sort and delete duplicates
  std::sort( active_hardware_interface.begin(), active_hardware_interface.end() );
  active_hardware_interface.erase( std::unique( active_hardware_interface.begin(), active_hardware_interface.end() ), active_hardware_interface.end() ); 
  
  
  std::multimap<std::string,std::string> next_configuration = get_controllers( m_config, req.start_configuration);
  ROS_DEBUG("Controllers to load:" );
  std::vector<std::string> next_robot_hardware;
  for(auto c : next_configuration )
  {
      ROS_DEBUG("\t- %s, %s", c.first.c_str(),  c.second.c_str());
      next_robot_hardware.push_back(c.first);
  }
  
  //sort and delete duplicates
  std::sort( next_robot_hardware.begin(), next_robot_hardware.end() );
  next_robot_hardware.erase( std::unique( next_robot_hardware.begin(), next_robot_hardware.end() ), next_robot_hardware.end() ); //delete duplicates
  
  ////////////////////////////////////////////////////////////////////////
  

  if (req.strictness==0)
    switch_ctrl_srv.request.strictness=1;
  else
    switch_ctrl_srv.request.strictness=req.strictness;
  

  std::vector<std::string> gazebo_robot_hw;
  for (std::string next: next_robot_hardware)
  {
    
    
    //check if already active
    if (std::find(active_hardware_interface.begin(), active_hardware_interface.end(), next) == active_hardware_interface.end())
    {
      ROS_DEBUG("[ Configuration Manager ] loading hardware interface: %s",next.c_str());
      if (!get_hardware_interface_load_info(next,load_srv.request))
      {
        ROS_ERROR("[ Configuration Manager ] hardware_interface %s parameters are missing",next.c_str());
        m_is_ok=false;
        res.ok=false;
        return true;
      }
      
      if (!load_srv.request.type.compare("gazebo"))
      {
        gazebo_robot_hw.push_back(next);
        continue;
      }
      
      // load of nodelet
      if (!m_load_nodelet.call(load_srv))
      {
        ROS_ERROR("[ Configuration Manager ] The service '%s' is broken. Failed during loading '%s'.",m_load_nodelet.getService().c_str(), next.c_str());
        m_is_ok=false;
        res.ok=false;
        return true;
      }
      
      if (!load_srv.response.success)
      {
        ROS_ERROR("[ Configuration Manager ] The service '%s' failed during loading '%s'.",m_load_nodelet.getService().c_str(), next.c_str());
        m_is_ok=false;
        return true;
      }
      
      std::shared_ptr<bond::Bond> bond_ptr =std::make_shared<bond::Bond>("manager/bond",load_srv.request.bond_id);
      bond_ptr->start();
      m_bonds.insert(std::pair<std::string,std::shared_ptr<bond::Bond>>(next,bond_ptr));
      ROS_INFO("hardware interface %s loaded",next.c_str());
    }
  }
  
  ////////////////////////////////////////////////////////////77
  // LOAD CONTROLLER
  nodelet::NodeletList list_nodelet_srv;
  if (!m_list_nodelet.call(list_nodelet_srv))
  {
    ROS_ERROR("[ Configuration Manager ] The service '%s' is broken. Abort.",m_list_nodelet.getService().c_str());
    m_is_ok=false;
    res.ok=false;
    return true;
  }
  
  for (const std::string& gazebo_hw: gazebo_robot_hw)
    list_nodelet_srv.response.nodelets.push_back(gazebo_hw);
    
  // for each hi, start new controllers and stop controllers
  for (std::string& hi: list_nodelet_srv.response.nodelets)
  {
    ros::ServiceClient load_ctrl   = m_nh.serviceClient<controller_manager_msgs::LoadController>  ("/"+hi+"/controller_manager/load_controller"  );
    ros::ServiceClient unload_ctrl = m_nh.serviceClient<controller_manager_msgs::UnloadController>("/"+hi+"/controller_manager/unload_controller");
    ros::ServiceClient switch_ctrl = m_nh.serviceClient<controller_manager_msgs::SwitchController>("/"+hi+"/controller_manager/switch_controller");
    ros::ServiceClient list_ctrls  = m_nh.serviceClient<controller_manager_msgs::ListControllers> ("/"+hi+"/controller_manager/list_controllers");
    load_ctrl.waitForExistence(ros::Duration(10));
    
    switch_ctrl_srv.request.start_controllers.resize(0);
    switch_ctrl_srv.request.stop_controllers.resize(0);

    // controller to be loaded
    std::vector<std::string> required_controllers;
    ROS_DEBUG("hardware_interface %s next controllers list:",hi.c_str());
    for (std::pair<std::string,std::string> ctrl : next_configuration)
    {
      if (!ctrl.first.compare(hi))
      {
        required_controllers.push_back(ctrl.second);
        ROS_DEBUG("  * %s",ctrl.second.c_str());
      }
    }
    
    if (!list_ctrls.waitForExistence(ros::Duration(4)))
    {
      ROS_ERROR("[ Configuration Manager ] Timeout expired (4s) while checking the status of '%s' . The service '%s' does not exist. Abort.", hi.c_str(), list_ctrls.getService().c_str() );
      m_is_ok=false;
      return true;
    }
    // controllers alreadt loaded
    controller_manager_msgs::ListControllers list_ctrls_srv;
    if (!list_ctrls.call(list_ctrls_srv))
    {
      
      ROS_ERROR("[ Configuration Manager ] The service '%s' is broken while checking the status of '%s' . Abort.", list_ctrls.getService().c_str(),hi.c_str() );
      m_is_ok=false;
      return true;
    }
    ROS_DEBUG("hardware_interface %s old controllers list:",hi.c_str());
    for (controller_manager_msgs::ControllerState& ctrl: list_ctrls_srv.response.controller)
    {
      ROS_DEBUG_STREAM(" " << ctrl.name << " ::" << ctrl.type << " ::" << ctrl.state);
    }
    
    
    // load new controller
    for (std::string& next_ctrl: required_controllers)
    {
      if (!next_ctrl.compare("NONE"))
        continue;
      
      bool is_already_present=false;
      bool is_already_started=false;
      for (controller_manager_msgs::ControllerState& ctrl: list_ctrls_srv.response.controller)
      {
        if (!ctrl.name.compare(next_ctrl))
        {
          is_already_present=true;
          is_already_started=!ctrl.state.compare("running");
          if (!is_already_started)
            switch_ctrl_srv.request.start_controllers.push_back(next_ctrl); // add to starting list
          else if (req.strictness==0) // force reloading
          {
            switch_ctrl_srv.request.stop_controllers.push_back(next_ctrl); 
            switch_ctrl_srv.request.start_controllers.push_back(next_ctrl);
          }
          break;
        }
      }
      
      if (!is_already_present)
      {
        
        load_ctrl_srv.request.name = next_ctrl;
        if (!load_ctrl.call( load_ctrl_srv) )
        {
          ROS_ERROR("[ Configuration Manager ] The service '%s' is broken while loading '%s' . Abort.", load_ctrl.getService().c_str(), next_ctrl.c_str() );
          m_is_ok=false;
          return true;
        }
        if (!load_ctrl_srv.response.ok)
        {
          ROS_ERROR("[ Configuration Manager ] The service '%s' failed while loading '%s'. Abort.", load_ctrl.getService().c_str(), next_ctrl.c_str() );
          m_is_ok=false;
          return true;
        }
        ROS_INFO("[ Configuration Manager ] CONTROLLER %s LOADED",next_ctrl.c_str());
        switch_ctrl_srv.request.start_controllers.push_back(next_ctrl); // add to starting list
      }
    }

    // check controllers to be stopped
    std::vector<std::string> not_required_controllers;
    for (controller_manager_msgs::ControllerState& ctrl: list_ctrls_srv.response.controller)
    {
      bool is_still_be_present=false;
      bool is_running=!ctrl.state.compare("running");
      for (std::string& next_ctrl: required_controllers)
      {
        if (!ctrl.name.compare(next_ctrl))
        {
          is_still_be_present=true;
          break;
        }
      }
      if (!is_still_be_present)
      {
        if (is_running)
        {
          switch_ctrl_srv.request.stop_controllers.push_back(ctrl.name);
          ROS_DEBUG("controller %s will be stop",ctrl.name.c_str());
        }
        not_required_controllers.push_back(ctrl.name);
        ROS_DEBUG("controller %s will be unloaded",ctrl.name.c_str());
      }
    }
    
    // switch controllers
    if (switch_ctrl_srv.request.start_controllers.size()==0 && switch_ctrl_srv.request.stop_controllers.size()==0)
    {
      ROS_DEBUG("HI=%s NOTHING TO DO",hi.c_str());
      continue;
    }

    
    // switch controller
    ROS_DEBUG("Switch controllers:");
    for (std::string& ctrl: switch_ctrl_srv.request.start_controllers)
      ROS_DEBUG("  ON  -> %s", ctrl.c_str());
    for (std::string& ctrl: switch_ctrl_srv.request.stop_controllers)
      ROS_DEBUG("  OFF <- %s", ctrl.c_str());
    
    if (!switch_ctrl.call(switch_ctrl_srv))
    {
      ROS_ERROR("[ Configuration Manager ] The service '%s' is broken. Abort.", switch_ctrl.getService().c_str() );
      m_is_ok=false;
      return true;
    }
    if (!switch_ctrl_srv.response.ok)
    {
      ROS_ERROR("[ Configuration Manager ] The service '%s' failed. Abort.", switch_ctrl.getService().c_str() );
      m_is_ok=false;
      return true;
    }
    
    /*
     * commented out the unloading of the controllers because with some configurations it was giving an error
     */
    
//     for (std::string& ctrl: not_required_controllers)
//     {
//       unload_ctrl_srv.request.name=ctrl;
//       if (!unload_ctrl.call(unload_ctrl_srv))
//       {
//         ROS_ERROR("Unaxpected Failure in unloading controller '%s'",ctrl.c_str());
//         m_is_ok=false;
//         return true;
//       }
//       if (!unload_ctrl_srv.response.ok)
//       {
//         ROS_ERROR("Failed unloading controller '%s'",ctrl.c_str());
//         return true;
//         m_is_ok=false;
//       }
//     }

    /*
     * END -- commented out the unloading of the controllers because with some configurations it was giving an error
     */
  }
  
  
  // unload unrequired hardware interfaces
  nodelet::NodeletUnload unload_srv;
  for (auto old: active_hardware_interface)
  {
    
    if (std::find(gazebo_robot_hw.begin(), gazebo_robot_hw.end(), old) != gazebo_robot_hw.end())
      continue;
    
    ROS_DEBUG("Check if '%s' can be unloaded",old.c_str());
    if (std::find(next_robot_hardware.begin(), next_robot_hardware.end(), old) == next_robot_hardware.end())
    {
      if (!m_list_nodelet.call(list_nodelet_srv))
      {
        ROS_ERROR("[ Configuration Manager ] The service '%s' is broken. Abort.", m_list_nodelet.getService().c_str() );
        m_is_ok=false;
        return true;
      }
      
      bool is_already_unloaded=true;
      for (const std::string& active_nodelet: list_nodelet_srv.response.nodelets)
        if (!active_nodelet.compare(old))
          is_already_unloaded=false;
      
      if (is_already_unloaded)
        continue;
      ROS_DEBUG("[ Configuration Manager ] Unloading hardware interface: %s",old.c_str());
      unload_srv.request.name=old;
      if (!m_unload_nodelet.call(unload_srv))
      {
        ROS_ERROR("[ Configuration Manager ] The service '%s' is broken. Abort.", m_unload_nodelet.getService().c_str() );
        m_is_ok=false;
        return true;
      }
      if (!unload_srv.response.success)
      {
        ROS_ERROR("[ Configuration Manager ] The service '%s' failed. Abort.", m_unload_nodelet.getService().c_str() );
        m_is_ok=false;
        return true;
      }
      m_bonds.erase(old);
    }
  }
  
  ROS_DEBUG("It is finished.");
  m_active_configuration=next_configuration;
  res.ok=true;
  m_is_ok=true;
  m_active_configuration_name=req.start_configuration;
  return true;
}

bool ConfigurationManager::stopCallback(configuration_msgs::StopConfiguration::Request& req, configuration_msgs::StopConfiguration::Response& res)
{
  configuration_msgs::StartConfiguration start_srv;
  start_srv.request.strictness=req.strictness;
  start_srv.request.start_configuration.clear();
  if (!startCallback(start_srv.request,start_srv.response))
    return false;
  res.ok=start_srv.response.ok;
  return true;
}

bool ConfigurationManager::listConfigurations(configuration_msgs::ListConfigurations::Request& req, configuration_msgs::ListConfigurations::Response& res)
{
  
  if ( m_config.getType() != XmlRpc::XmlRpcValue::TypeArray )
  {
    ROS_ERROR ( "controllers list is not a struct");
    return true;
  }
  
  for(size_t i=0; i < m_config.size(); i++) 
  {
    XmlRpc::XmlRpcValue controller = m_config[i];  
    if( controller.getType() != XmlRpc::XmlRpcValue::TypeStruct)
    {
      ROS_WARN("The element #%zu is not a struct", i);
      continue;
    }
    if( !controller.hasMember("name") )
    {
      ROS_WARN("The element #%zu has not the field 'name'", i);
      continue;
    }
    if( !controller.hasMember( "components" ) )
    {
      ROS_WARN("The element #%zu has not the field 'components'", i);
      continue;
    }
    if( controller["components"].getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_WARN("The element #%zu has 'components' bad-formed", i);
      continue;
    }
    
    XmlRpc::XmlRpcValue components = controller["components"];
    
    configuration_msgs::ConfigurationComponent cs;
    
    cs.name=(std::string)controller["name"];
    
    if (!m_active_configuration_name.compare(cs.name))
    {
      cs.state="running";
    }
    else
    {
      cs.state="loaded";
    }
    
    cs.hardware_interfaces.resize(0);
    cs.controllers.resize(0); 
    for( size_t i=0; i<components.size();i++)
    {
      
      if( ( components[i].getType() == XmlRpc::XmlRpcValue::TypeStruct)
        &&  ( components[i].hasMember("hardware_interface") )
        &&  ( components[i].hasMember("controller") ) )
      {
        cs.hardware_interfaces.push_back( (std::string)components[i]["hardware_interface"]);
        cs.controllers.push_back(         (std::string)components[i]["controller"]);
      }
    }
    res.configurations.push_back(cs);
  }
  return true;
  
}

bool ConfigurationManager::checkBondStatus()
{
  
  for (std::pair<std::string,std::shared_ptr<bond::Bond>> hi_bond: m_bonds)
  {
    if (hi_bond.second->isBroken())
    {
      ROS_WARN("hardware_interface %s is shutted down",hi_bond.first.c_str());
      return false;
    }
  }
  return true;
}

bool ConfigurationManager::purgeBond()
{
  bool is_bond_broken=false;
  std::vector<std::string> broken_bonds;
  for (std::pair<std::string,std::shared_ptr<bond::Bond>> hi_bond: m_bonds)
  {
    if (hi_bond.second->isBroken())
    {
      ROS_WARN("hardware_interface %s is shutted down, removing bond",hi_bond.first.c_str());
      is_bond_broken=true;
      broken_bonds.push_back(hi_bond.first);
    }
  }
  if (is_bond_broken)
    for (const std::string& name: broken_bonds)
      m_bonds.at(name).reset();
  return is_bond_broken;
}



ConfigurationManager::~ConfigurationManager()
{
//   for (auto const &entry: m_active_configuration)
//   {
//     nodelet::NodeletUnload unload_srv;
//     ROS_DEBUG("Unloading hardware interface: %s",entry.first.c_str());
//     unload_srv.request.name = entry.first;
//     if(!m_unload_nodelet.call(unload_srv))
//     {
//       ROS_ERROR("Failed unloading hardware interface");
//     }
//   }
  
}


}
