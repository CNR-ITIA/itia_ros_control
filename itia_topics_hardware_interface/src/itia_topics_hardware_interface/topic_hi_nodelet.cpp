#include <itia_topics_hardware_interface/topic_hi_nodelet.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::TopicsHwIfaceNodelet, nodelet::Nodelet) 

#define FATAL_THROW( MSG )\
      ROS_FATAL("ERROR DURING STARTING HARDWARE INTERFACE '%s': %s", getPrivateNodeHandle().getNamespace().c_str(), std::string( MSG ).c_str() );\
      std::cout.flush();\
      throw std::runtime_error( ("Exception: ERROR DURING STARTING HARDWARE INTERFACE '" + getPrivateNodeHandle().getNamespace() +"': " + std::string( MSG ) ).c_str()  )

#define WARNING( MSG )\
      ROS_WARN("WARNING DURING STARTING HARDWARE INTERFACE '%s/%s", getPrivateNodeHandle().getNamespace().c_str(), std::string( MSG ).c_str() );\
      
      
namespace itia
{
  namespace control
  {
    void TopicsHwIfaceNodelet::onInit()
    {
      bool verbose;
      if( ros::NodeHandle("~").hasParam("verbose") )
      {
        ros::NodeHandle("~").getParam("verbose", verbose);
        if(verbose) 
          ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
      }
      
          
      std::vector<std::string> resources;
      if (!getPrivateNodeHandle().getParam("resources", resources))
      {
        FATAL_THROW( getPrivateNodeHandle().getNamespace()+"/resources' does not exist" );
      }
      
      int maximum_missing_messages;
      if( !getPrivateNodeHandle().getParam( "maximum_missing_messages", maximum_missing_messages) )
      {
        WARNING( getPrivateNodeHandle().getNamespace()+"/maximum_missing_messages does not exist" );
      }
      
      
      std::map< itia_hardware_interface::RESOURCE_ID, std::shared_ptr< itia_hardware_interface::Resource > > claimed_resources;
      
      for( auto const & resource : resources )
      {
        std::map< itia_hardware_interface::RESOURCE_ID, std::string >::const_iterator it = itia_hardware_interface::RESOURCES().begin();
        
        for( it = itia_hardware_interface::RESOURCES().begin() ; it !=  itia_hardware_interface::RESOURCES().end(); it++ )
        {
          if( it->second == resource )
          {
            break;    
          }
        }
        
        
        if( it != itia_hardware_interface::RESOURCES().end() )
        {
          ROS_INFO("Reading param for resource: '%s'", it->second.c_str() );
          std::string ns = getPrivateNodeHandle().getNamespace() + "/" + it->second;
          
          std::shared_ptr< itia_hardware_interface::Resource > claimed_resource;
          switch( it->first )
          {

            case itia_hardware_interface::JOINT_RESOURCE: 
            {
              
              std::shared_ptr< itia_hardware_interface::JointResource > jr( new itia_hardware_interface::JointResource( ) );
              std::vector<std::string>  joint_names;
              ROS_INFO("joint_names" );
              if( !getPrivateNodeHandle().getParam( it->second + "/joint_names", joint_names) )
              {
                FATAL_THROW( ns +"/joint_names does not exist" );
              }
              
              jr->m_joint_names = joint_names;
              
              jr->m_nAx = joint_names.size( );
              if( joint_names.size( ) == 0 )
              {
                
                FATAL_THROW( ns +"/joint_names has size zero" );
                
              }
              claimed_resource = jr;
              
            }
            break;
            case itia_hardware_interface::WRENCH_RESOURCE: 
            {
              
              std::shared_ptr< itia_hardware_interface::ForceTorqueResource > wr( new itia_hardware_interface::ForceTorqueResource( ) );
              std::string sensor_name;
              if( !getPrivateNodeHandle().getParam( it->second + "/sensor_name", sensor_name ) )
              {
                FATAL_THROW( ns +"/sensor_name does not exist" );
              }
              std::string frame_id;
              if( !getPrivateNodeHandle().getParam( it->second + "/frame_id", frame_id ) )
              {
                FATAL_THROW( ns +"/frame_id does not exist" );
              }
              
              wr->m_sensor_name = sensor_name;
              wr->m_frame_id    = frame_id;
              claimed_resource = wr;
            }
            break;
            case itia_hardware_interface::ANALOG_RESOURCE: 
            { 
              std::shared_ptr< itia_hardware_interface::AnalogResource > ar( new itia_hardware_interface::AnalogResource( ) );
              std::vector< std::string > channel_names;
              if( !getPrivateNodeHandle().getParam( it->second + "/channel_names", channel_names ) )
              {
                FATAL_THROW( ns +"/channel_names does not exist" );
              }
              ar->m_channel_names = channel_names;
              ar->m_num_channels = ar->m_channel_names.size();
              claimed_resource = ar;
            }
            break;
          }
          
          std::string published_topic = "N/A";
          if( !getPrivateNodeHandle().getParam( it->second + "/published_topic", published_topic) )
          {
            WARNING( it->second + +"/published_topic does not exist" );
          }
          else
          {
            std::cout << " published topics:" << published_topic << std::endl;
          }
          claimed_resource->m_published_topic = published_topic;
          
          std::vector< std::string > subscribed_topics;
          if( !getPrivateNodeHandle().getParam( it->second + "/subscribed_topics", subscribed_topics ) )
          {
            WARNING( it->second + +"/subscribed_topics does not exist" );
            subscribed_topics.clear();
          }
          else
          {
            std::cout << " subscribed topics: " << std::endl;
            for(auto const & s : subscribed_topics )
              std::cout << " - " << s << std::endl;
          }
          
          
          double feedback_joint_state_timeout_s;
          if( !getPrivateNodeHandle().getParam( it->second + "/feedback_joint_state_timeout_s", published_topic) )
          {
            WARNING( it->second + +"/feedback_joint_state_timeout_s does not exist" );
          }
          claimed_resource->m_feedback_joint_state_timeout_s = feedback_joint_state_timeout_s;
          claimed_resource->m_subscribed_topics = subscribed_topics;
          
          claimed_resources[ it->first ] = claimed_resource;
        }
        else
        {
          ROS_WARN_STREAM( getPrivateNodeHandle().getNamespace() << "/resources/" <<  resource << "'  is not supported");
          ROS_WARN_STREAM( " Available Resource: " << itia_hardware_interface::AVAILABLE_RESOURCES( ) );
        }
        
      }
      if( claimed_resources.size() == 0 )
      {
        FATAL_THROW("No claimed resources. ?!?!?");
      }
      
      m_hw.reset(new itia_hardware_interface::TopicsRobotHW(claimed_resources, maximum_missing_messages));
      
      startThreads();
      
    };
    
    
  }
}
#undef FATAL_THROW
#undef WARNING
