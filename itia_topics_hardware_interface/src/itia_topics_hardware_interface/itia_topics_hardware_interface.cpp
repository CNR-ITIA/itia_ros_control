
#include <geometry_msgs/PoseStamped.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <name_sorting/name_sorting.h>


#include <itia_topics_hardware_interface/itia_topics_hardware_interface.h>

static size_t line = __LINE__;
#define __LL__ line = __LINE__;


namespace itia_hardware_interface
{
  
TopicsRobotHW::TopicsRobotHW( std::map< itia_hardware_interface::RESOURCE_ID, std::shared_ptr< itia_hardware_interface::Resource > > resources, unsigned int maximum_missing_messages)
: m_resources( resources ), m_joint_resource( nullptr ), m_missing_messages(0), m_max_missing_messages(maximum_missing_messages)
{
}

bool TopicsRobotHW::init(ros::NodeHandle& root_nh, ros::NodeHandle& robot_hw_nh)
{
  try
  {
__LL__;    ROS_INFO("[%s] TopicsRobotHW initialization",robot_hw_nh.getNamespace().c_str());
__LL__;    
__LL__;    if (!itia_hardware_interface::BasicRobotHW::init(root_nh, robot_hw_nh))
           {
__LL__;      ROS_ERROR("[%s] BasicRobotHW error",robot_hw_nh.getNamespace().c_str());
__LL__;      return false;
           }
__LL__;    
__LL__;    
__LL__;    if( m_resources.count( JOINT_RESOURCE ) )
           {
__LL__;      ROS_INFO("[%s] Crate Joint Claimed Resource",robot_hw_nh.getNamespace().c_str());
__LL__;      std::shared_ptr< itia_hardware_interface::Resource > p = m_resources.at( JOINT_RESOURCE );
__LL__;      std::shared_ptr< itia_hardware_interface::JointResource > jp = std::static_pointer_cast<itia_hardware_interface::JointResource>( p );
__LL__;      m_joint_resource.reset( new JointClaimedResource( *jp, robot_hw_nh, this->m_topics_subscribed ) );
           }
__LL__;    
__LL__;    if( m_resources.count( ANALOG_RESOURCE ) )
           {
__LL__;      ROS_INFO("[%s] Crate Analog Claimed Resource",robot_hw_nh.getNamespace().c_str());
__LL__;      std::shared_ptr< itia_hardware_interface::Resource > p = m_resources.at( ANALOG_RESOURCE );
__LL__;      std::shared_ptr< itia_hardware_interface::AnalogResource > ap = std::static_pointer_cast<itia_hardware_interface::AnalogResource>( p );
__LL__;      m_analog_resource.reset( new AnalogClaimedResource( *ap, robot_hw_nh, this->m_topics_subscribed ) );
           }
__LL__;    
__LL__;    
__LL__;    if( m_resources.count( WRENCH_RESOURCE ) )
           {
__LL__;      ROS_INFO("[%s] Crate Force Torque Sensor Claimed Resource",robot_hw_nh.getNamespace().c_str());
__LL__;      std::shared_ptr< itia_hardware_interface::Resource > p = m_resources.at( WRENCH_RESOURCE );
__LL__;      std::shared_ptr< itia_hardware_interface::ForceTorqueResource > wp = std::static_pointer_cast<itia_hardware_interface::ForceTorqueResource>( p );
__LL__;      auto a =  new ForceTorqueClaimedResource( *wp, robot_hw_nh, this->m_topics_subscribed );
__LL__;      m_force_torque_sensor_resource.reset( a );
           }
__LL__;    
__LL__;    if( m_resources.count( JOINT_RESOURCE ) )
           { 
__LL__;      ROS_INFO("[%s] Init Joint Claimed Resource",robot_hw_nh.getNamespace().c_str());
__LL__;      initJointClaimedResource( );
           }
__LL__;    if( m_resources.count( ANALOG_RESOURCE ) )
           { 
__LL__;      ROS_INFO("[%s] Init Analog Claimed Resource",robot_hw_nh.getNamespace().c_str());
__LL__;      initAnalogClaimedResource( );
           }
__LL__;    if( m_resources.count( WRENCH_RESOURCE ) )
           { 
__LL__;      ROS_INFO("[%s] Init Force Torque Sensor Resource",robot_hw_nh.getNamespace().c_str());
__LL__;      initForceTorqueClaimedResource( );
           }
__LL__;      
__LL__;    ROS_INFO("[%s] Ok, TopicsRobotHW initialized",robot_hw_nh.getNamespace().c_str());
  }
  catch(std::exception& e)
  {
    ROS_FATAL("TopicsRobotHW error %s (at line:%zu)",e.what(), line);
    std::cout.flush();
    fflush(stdout); 
    fflush(stderr); 
    return false;
  }
  catch(...)
  {
    ROS_FATAL("TopicsRobotHW error. Unhandled expcetion at line:%zu",line);
    std::cout.flush();
    fflush(stdout); 
    fflush(stderr); 
    return false;
  }
  
  return true;
}

void TopicsRobotHW::read(const ros::Time& time, const ros::Duration& period)
{
  m_queue.callAvailable();
  if (!topicsReceived() )
  {
    m_missing_messages++;
  }
  else
  {
    m_missing_messages=0;
  }
  
  resetTopicsReceived();
  
  if (m_missing_messages > m_max_missing_messages)
  {
    ROS_ERROR("[%s] maximum_missing_messages (%d) reached ",m_robot_hw_nh.getNamespace().c_str(),m_missing_messages);
    m_status=with_error;
    diagnostic_msgs::DiagnosticStatus diag;
    diag.name=m_robot_hw_nh.getNamespace();
    diag.hardware_id=m_robot_hw_nh.getNamespace();
    diag.level=diagnostic_msgs::DiagnosticStatus::ERROR;
    diag.message="Hardware interface "+m_robot_hw_nh.getNamespace()+" run time: no feeback messages received";
    m_diagnostic.status.push_back(diag);
  }
}

void TopicsRobotHW::write(const ros::Time& time, const ros::Duration& period)
{
  if( m_resources.count( JOINT_RESOURCE ) )
  {
    assert( m_joint_resource );
    m_joint_resource->write(time, period);
  }
  if( m_resources.count( ANALOG_RESOURCE ) )
  {
    assert( m_analog_resource );
      m_analog_resource->write(time, period);
  }
  if( m_resources.count( WRENCH_RESOURCE ) )
  {
    assert( m_force_torque_sensor_resource );
    m_force_torque_sensor_resource->write(time, period);
  }
}

bool TopicsRobotHW::prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
  if( m_resources.count( JOINT_RESOURCE ) )
  {
    assert( m_joint_resource );
    m_joint_resource->prepareSwitch(start_list, stop_list);
  }
  if( m_resources.count( ANALOG_RESOURCE) )
  {
    assert( m_analog_resource );
    m_analog_resource->prepareSwitch(start_list, stop_list);
  }
  if( m_resources.count( WRENCH_RESOURCE) )
  {
    assert( m_force_torque_sensor_resource );
    m_force_torque_sensor_resource->prepareSwitch(start_list, stop_list);
  }
  return true;
}

void TopicsRobotHW::doSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
}

bool TopicsRobotHW::checkForConflict(const std::list< hardware_interface::ControllerInfo >& info) 
{
  
  if( m_resources.count( JOINT_RESOURCE ) )
  {
    assert( m_joint_resource );
    if( m_joint_resource->checkForConflict(info) )
    {
      ROS_ERROR("Joint Joint Resource is in conflict with another controller" );
      diagnostic_msgs::DiagnosticStatus diag;
      diag.name=m_robot_hw_nh.getNamespace();
      diag.hardware_id=m_robot_hw_nh.getNamespace();
      diag.level=diagnostic_msgs::DiagnosticStatus::ERROR;
      diag.message="Hardware interface "+m_robot_hw_nh.getNamespace()+" run time: Joint Resource";
      m_diagnostic.status.push_back(diag);
    }
  }
  if( m_resources.count( ANALOG_RESOURCE ) )
  {
    assert( m_analog_resource );
    if( m_analog_resource->checkForConflict(info) )
    {
      ROS_ERROR("Analog Resource is in conflict with another controller" );
      diagnostic_msgs::DiagnosticStatus diag;
      diag.name=m_robot_hw_nh.getNamespace();
      diag.hardware_id=m_robot_hw_nh.getNamespace();
      diag.level=diagnostic_msgs::DiagnosticStatus::ERROR;
      diag.message="Hardware interface "+m_robot_hw_nh.getNamespace()+" run time: Analog Resource";
      m_diagnostic.status.push_back(diag);
    }
  }
  
  if( m_resources.count( WRENCH_RESOURCE ) )
  {
    assert( m_force_torque_sensor_resource );
    if( m_force_torque_sensor_resource->checkForConflict(info) )
    {

      ROS_ERROR("FT Resource is in conflict with another controller" );
      diagnostic_msgs::DiagnosticStatus diag;
      diag.name=m_robot_hw_nh.getNamespace();
      diag.hardware_id=m_robot_hw_nh.getNamespace();
      diag.level=diagnostic_msgs::DiagnosticStatus::ERROR;
      diag.message="Hardware interface "+m_robot_hw_nh.getNamespace()+" run time: FT Resource";
      m_diagnostic.status.push_back(diag);
    }
  }
  
  return false;
}

void TopicsRobotHW::shutdown()
{
  if( m_resources.count( JOINT_RESOURCE ) )
  {
    assert( m_joint_resource );
    m_joint_resource->shutdown();
  }
  if( m_resources.count( JOINT_RESOURCE ) )
  {
    assert( m_analog_resource );
    m_analog_resource->shutdown();
  }
  if( m_resources.count( WRENCH_RESOURCE ) )
  {
    assert( m_analog_resource );
    m_analog_resource->shutdown();
  }
}

bool TopicsRobotHW::initJointClaimedResource( )
{
  bool ret = true;
  if( m_resources.count( JOINT_RESOURCE ) )
  {
    assert( m_joint_resource );
    for (std::string& joint_name: m_joint_resource->m_resource_names) 
    {
      
      auto i = &joint_name-&(m_joint_resource->m_resource_names[0]);
      
      hardware_interface::JointStateHandle state_handle(joint_name, 
                                                        &(m_joint_resource->m_pos.at(i)), 
                                                        &(m_joint_resource->m_vel.at(i)), 
                                                        &(m_joint_resource->m_eff.at(i)));
      
      
      m_joint_resource->m_js_jh.registerHandle(state_handle);
      
      m_joint_resource->m_p_jh.registerHandle( hardware_interface::JointHandle(state_handle, &(m_joint_resource->m_cmd_pos.at(i))) );
      m_joint_resource->m_v_jh.registerHandle( hardware_interface::JointHandle(state_handle, &(m_joint_resource->m_cmd_vel.at(i))) );
      m_joint_resource->m_e_jh.registerHandle( hardware_interface::JointHandle(state_handle, &(m_joint_resource->m_cmd_eff.at(i))) );
      
      m_joint_resource->m_pve_jh.registerHandle(hardware_interface::PosVelEffJointHandle(state_handle,&(m_joint_resource->m_cmd_pos.at(i)),&(m_joint_resource->m_cmd_vel.at(i)),&(m_joint_resource->m_cmd_eff.at(i))));
      m_joint_resource->m_ve_jh.registerHandle(hardware_interface::VelEffJointHandle(state_handle,&(m_joint_resource->m_cmd_vel.at(i)),&(m_joint_resource->m_cmd_eff.at(i))));
    }
    
    registerInterface(&m_joint_resource->m_js_jh);
    registerInterface(&m_joint_resource->m_p_jh);
    registerInterface(&m_joint_resource->m_v_jh);
    registerInterface(&m_joint_resource->m_e_jh);
    registerInterface(&m_joint_resource->m_pve_jh);
    registerInterface(&m_joint_resource->m_ve_jh);
    
    m_joint_resource->m_p_jh_active = m_joint_resource->m_v_jh_active = m_joint_resource->m_e_jh_active = false;
    
    m_joint_resource->m_msg.reset(new sensor_msgs::JointState());
    m_joint_resource->m_msg->name         = m_joint_resource->m_resource_names;
    m_joint_resource->m_msg->position     = m_joint_resource->m_pos;
    m_joint_resource->m_msg->velocity     = m_joint_resource->m_vel;
    m_joint_resource->m_msg->effort.resize( m_joint_resource->m_nAx );
    std::fill(m_joint_resource->m_msg->effort.begin(),m_joint_resource->m_msg->effort.end(),0.0);
  }
  
    
  return ret;
}

bool TopicsRobotHW::initAnalogClaimedResource( )
{
  bool ret = true;
  if( m_resources.count( ANALOG_RESOURCE ) )
  {
    assert( m_analog_resource );
    for (std::string& channel_name: m_analog_resource->m_resource_names) 
    {
      auto i = &channel_name - &(m_analog_resource->m_resource_names[0]);
      
      hardware_interface::AnalogStateHandle state_handle( channel_name, &(m_analog_resource->m_state.at(i)));
      
      m_analog_resource->m_a_sh.registerHandle(state_handle);
      m_analog_resource->m_a_h.registerHandle( hardware_interface::AnalogHandle (state_handle, &(m_analog_resource->m_output.at(i) ) ) );
    }
    
    registerInterface(&m_analog_resource->m_a_h);
    registerInterface(&m_analog_resource->m_a_sh);
    
    m_analog_resource->m_a_sh_active = m_analog_resource->m_a_h_active = false;
    
    m_analog_resource->m_msg.reset(new std_msgs::Float64MultiArray());
    m_analog_resource->m_msg->data.resize(  m_analog_resource->m_resource_names.size(), 0);
    
  }
    
  return ret;
}


bool TopicsRobotHW::initForceTorqueClaimedResource( )
{
  bool ret = false;
  if( m_resources.count( WRENCH_RESOURCE ) )
  {
    assert( m_force_torque_sensor_resource );
    
    hardware_interface::ForceTorqueStateHandle state_handle ( m_force_torque_sensor_resource->m_resource_names.front()
                                                            , m_force_torque_sensor_resource->m_frame_id
                                                            , &( m_force_torque_sensor_resource->m_state.at(0) )
                                                            , &( m_force_torque_sensor_resource->m_state.at(3) ) );
                                                            
    m_force_torque_sensor_resource->m_w_sh.registerHandle(state_handle);
    m_force_torque_sensor_resource->m_w_h.registerHandle( hardware_interface::ForceTorqueHandle( state_handle, &( m_force_torque_sensor_resource->m_output.at(0) ), &( m_force_torque_sensor_resource->m_output.at(3) ) ) );
    
    registerInterface(&m_force_torque_sensor_resource->m_w_sh);
    registerInterface(&m_force_torque_sensor_resource->m_w_h);
    
    m_force_torque_sensor_resource->m_w_sh_active = m_force_torque_sensor_resource->m_w_h_active = false;
    
    m_force_torque_sensor_resource->m_msg.reset(new geometry_msgs::WrenchStamped() );
    
    ret = true;
  }
    
  return ret;

}


}

