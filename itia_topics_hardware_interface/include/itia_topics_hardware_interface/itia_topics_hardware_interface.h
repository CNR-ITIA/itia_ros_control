#ifndef __ITIA_TOPIC_HARDWARE_INTERFACE__
#define __ITIA_TOPIC_HARDWARE_INTERFACE__

#include <name_sorting/name_sorting.h>

#include <itia_basic_hardware_interface/itia_basic_hardware_interface.h>
#include <itia_basic_hardware_interface/posveleff_command_interface.h>

#include <itia_basic_hardware_interface/force_torque_state_interface.h>
#include <itia_basic_hardware_interface/force_torque_command_interface.h>

#include <itia_basic_hardware_interface/analog_state_interface.h>
#include <itia_basic_hardware_interface/analog_comand_interface.h>

#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/WrenchStamped.h>
#include <mutex>


namespace itia_hardware_interface
{
  enum RESOURCE_ID
  {
    JOINT_RESOURCE, ANALOG_RESOURCE, WRENCH_RESOURCE
  };
  
  inline const std::map<RESOURCE_ID, std::string>& RESOURCES( )
  {
    static std::map<RESOURCE_ID, std::string> ret ={ {JOINT_RESOURCE , "joint_resource" }, {ANALOG_RESOURCE, "analog_resource" }, {WRENCH_RESOURCE, "wrench_resource" } };
    return ret;
  }
  inline std::string AVAILABLE_RESOURCES( )
  {
    std::string ret;
    for( auto const & p : RESOURCES() )
      ret += p.second + ", ";
    return ret;
  }
  
  struct Resource
  {
    std::vector<std::string>  m_subscribed_topics;
    std::string               m_published_topic;
    double                    m_feedback_joint_state_timeout_s;
  };

  struct JointResource : Resource
  {
    std::vector<std::string>  m_joint_names;
    unsigned int              m_nAx;
  };

  struct ForceTorqueResource : Resource
  {
    std::string m_sensor_name;
    std::string m_frame_id;
  };

  struct AnalogResource : Resource
  {
    std::vector<std::string>  m_channel_names;
    int         m_num_channels;
  };
  
  template < typename MSG >
  struct ClaimedResource 
  {
    ClaimedResource() = delete;
    ClaimedResource( const itia_hardware_interface::Resource& res, const std::vector<std::string> resource_names,  ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received )
    : m_console( robot_hw_nh.getNamespace() ), m_topics_received( topics_received ), m_pub(nullptr), m_resource_names( resource_names ), m_msg_counter(0)
    { 
      size_t l = __LINE__;;
      try 
      {
        l = __LINE__;
        for( size_t i=0; i<res.m_subscribed_topics.size(); i++) 
        { 
          m_topics_received [ res.m_subscribed_topics.at(i) ] = false;
        }
        l = __LINE__;
        for( const std::string & subscribed_topic : res.m_subscribed_topics ) 
        {
          std::shared_ptr< ros::Subscriber > sub( new ros::Subscriber() );
          *sub = robot_hw_nh.subscribe< MSG >( subscribed_topic, 1, boost::bind( &itia_hardware_interface::ClaimedResource< MSG >::callback,this, _1, subscribed_topic ) );
          m_sub.push_back( sub );
        }
        
        l = __LINE__;
        if( res.m_published_topic != "N/A" )
        {
          m_pub.reset( new ros::Publisher() );
          *m_pub = robot_hw_nh.advertise< MSG >(res.m_published_topic,1);
        }
        
        l = __LINE__;
        for( size_t i=0; i<res.m_subscribed_topics.size(); i++)
        {
          const std::string & t = res.m_subscribed_topics.at(i);
          m_idxes_ax_map        [ t ].push_back( i );
          m_resource_names_map  [ t ].push_back( m_resource_names.at( i ) ); 
        }
      } 
      catch(std::exception& e)
      {
        ROS_FATAL("ClaimedResource error %s (at line:%zu)", e.what(), l);
        std::cout.flush();
        fflush(stdout); 
        fflush(stderr); 
        throw std::runtime_error( ("ClaimedResource error"  + std::string( e.what() ) ) .c_str() );
      }
      catch(...)
      {
        ROS_FATAL("ClaimedResource error. Unhandled expcetion at line:%zu",l);
        std::cout.flush();
        fflush(stdout); 
        fflush(stderr); 
        throw std::runtime_error( "ClaimedResource error"  );
      }
    }
    
    ~ClaimedResource()
    {
      m_mutex.lock();
      m_mutex.unlock();
    }
    
    virtual void write   (const ros::Time& time, const ros::Duration& period) 
    {
      if( m_pub )
      {
        m_mutex.lock();
        m_pub->publish(m_msg);
        m_mutex.unlock();
        
        typename MSG::Ptr msg(new MSG());
        m_msg.swap(msg);
      }
      
    }
    /*bool prepareSwitch   (const std::list<hardware_interface::ControllerInfo>& start_list,const std::list<hardware_interface::ControllerInfo>& stop_list) = 0;*/
    virtual void callback(const typename MSG::ConstPtr& msg, const std::string& topic ) { m_msg_counter++; };
    void shutdown        ( )  { if( m_pub ) m_pub.reset(); }
    bool checkForConflict(const std::list< hardware_interface::ControllerInfo >& info) 
    {
      std::vector<bool> global_joint_used(m_resource_names.size()); 
      std::fill(global_joint_used.begin(),global_joint_used.end(),false);
      
      for (hardware_interface::ControllerInfo controller: info)
      {
        std::vector<bool> single_controller_joint_used(m_resource_names.size()); 
        std::fill(single_controller_joint_used.begin(),single_controller_joint_used.end(),false);
        
        for (hardware_interface::InterfaceResources res: controller.claimed_resources)
        {
          for (std::string name: res.resources)
          {
            for (unsigned int iJ=0;iJ<m_resource_names.size();iJ++)
            {
              if (!name.compare(m_resource_names.at(iJ)))
              {
                if (global_joint_used.at(iJ)) // if already used by another
                {
                  return true;
                }
                else
                  single_controller_joint_used.at(iJ);
              }
            }
          }
        }
        for (unsigned int iJ=0;iJ<m_resource_names.size(); iJ++)
          global_joint_used.at(iJ)= global_joint_used.at(iJ) || single_controller_joint_used.at(iJ);
        
      }
      return false;
    }

    const std::string                                    m_console;
    std::map< std::string, bool>&                        m_topics_received;
    std::vector< std::shared_ptr<ros::Subscriber> >      m_sub;
    std::shared_ptr<ros::Publisher>                      m_pub;
    typename MSG::Ptr                                    m_msg;
    std::vector<std::string>                             m_resource_names;
    std::map< std::string, std::vector<std::string   > > m_resource_names_map;
    std::map< std::string, std::vector< unsigned int > > m_idxes_ax_map;

    std::mutex                                      m_mutex;
    size_t                                          m_msg_counter;

  };
    
  struct JointClaimedResource : ClaimedResource< sensor_msgs::JointState >
  {
    JointClaimedResource() = delete;
    JointClaimedResource( const itia_hardware_interface::JointResource& jr, ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received );
                        
    
    void shutdown();
    void write            (const ros::Time& time, const ros::Duration& period);
    bool prepareSwitch    (const std::list<hardware_interface::ControllerInfo>& start_list,const std::list<hardware_interface::ControllerInfo>& stop_list);
    void callback         (const sensor_msgs::JointStateConstPtr& msg, const std::string& topic );

    hardware_interface::JointStateInterface         m_js_jh; //interface for reading joint state
    hardware_interface::PositionJointInterface      m_p_jh; //interface for writing position target
    hardware_interface::VelocityJointInterface      m_v_jh; //interface for writing velocity target
    hardware_interface::EffortJointInterface        m_e_jh; //interface for writing effort target
    hardware_interface::PosVelEffJointInterface     m_pve_jh;
    hardware_interface::VelEffJointInterface        m_ve_jh;
    
    bool m_p_jh_active;
    bool m_v_jh_active;
    bool m_e_jh_active;
    
    
    std::vector<double> m_pos; // feedback position
    std::vector<double> m_vel; // feedback velocity
    std::vector<double> m_eff; // feedback effort
    
    std::vector<double> m_cmd_pos; //target position
    std::vector<double> m_cmd_vel; //target velocity
    std::vector<double> m_cmd_eff; //target effort
    
    unsigned int m_nAx;
  };
  
  struct AnalogClaimedResource : ClaimedResource< std_msgs::Float64MultiArray >
  {
    AnalogClaimedResource() = delete;
    AnalogClaimedResource( const itia_hardware_interface::AnalogResource& ar, ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received );
    
    void write            (const ros::Time& time, const ros::Duration& period);
    bool prepareSwitch    (const std::list<hardware_interface::ControllerInfo>& start_list,const std::list<hardware_interface::ControllerInfo>& stop_list);
    void callback         (const std_msgs::Float64MultiArray::ConstPtr& msg, const std::string& topic );

    hardware_interface::AnalogStateInterface   m_a_sh; //interface for reading joint state
    hardware_interface::AnalogCommandInterface m_a_h;

    bool                                       m_a_sh_active;
    bool                                       m_a_h_active;

    std::vector<double>                        m_state;  // subscribed
    std::vector<double>                        m_output; // published
  };
  
  struct ForceTorqueClaimedResource : ClaimedResource< geometry_msgs::WrenchStamped >
  {
    ForceTorqueClaimedResource() = delete;
    ForceTorqueClaimedResource( const itia_hardware_interface::ForceTorqueResource& ar, ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received );
    
    void write            (const ros::Time& time, const ros::Duration& period);
    bool prepareSwitch    (const std::list<hardware_interface::ControllerInfo>& start_list,const std::list<hardware_interface::ControllerInfo>& stop_list);
    void callback         (const geometry_msgs::WrenchStamped::ConstPtr& msg, const std::string& topic );

    hardware_interface::ForceTorqueStateInterface  m_w_sh; //interface for reading joint state
    hardware_interface::ForceTorqueInterface       m_w_h;

    bool                                        m_w_sh_active;
    bool                                        m_w_h_active;

    std::vector<double>                         m_state;  // subscribed
    std::vector<double>                         m_output; // published
    
    std::string                                 m_frame_id;
  };
  /**
   * 
   * 
   * 
   * @class TopicsRobotHW
   * 
   * 
   * 
   */
  
  class TopicsRobotHW: public itia_hardware_interface::BasicRobotHW
  {
  public:
    TopicsRobotHW( std::map< itia_hardware_interface::RESOURCE_ID, std::shared_ptr< itia_hardware_interface::Resource > > resources, unsigned int maximum_missing_messages);
    virtual void shutdown();
    virtual void read(const ros::Time& time, const ros::Duration& period);
    virtual void write(const ros::Time& time, const ros::Duration& period);
    
    virtual bool init             (ros::NodeHandle& root_nh, ros::NodeHandle &robot_hw_nh) ;
    virtual bool checkForConflict (const std::list<hardware_interface::ControllerInfo>& info) ;
    virtual bool prepareSwitch    (const std::list<hardware_interface::ControllerInfo>& start_list,const std::list<hardware_interface::ControllerInfo>& stop_list);
    virtual void doSwitch         (const std::list<hardware_interface::ControllerInfo>& start_list,const std::list<hardware_interface::ControllerInfo>& stop_list);
    
  protected:
    
    std::map< RESOURCE_ID, std::shared_ptr< itia_hardware_interface::Resource > > m_resources;
    
    
    std::shared_ptr< JointClaimedResource > m_joint_resource;
    bool initJointClaimedResource( );

    std::shared_ptr< AnalogClaimedResource > m_analog_resource;
    bool initAnalogClaimedResource( );
    
    
    std::shared_ptr< ForceTorqueClaimedResource > m_force_torque_sensor_resource;
    bool initForceTorqueClaimedResource( );

    
    enum status { created, initialized, run, error };
    
    unsigned int                 m_missing_messages;
    const unsigned int           m_max_missing_messages;
    std::map< std::string, bool> m_topics_subscribed;
    
    bool allSubscriberConnected( ) const 
    {
        bool all_topics_received = true;
        for( const std::pair<std::string,bool>& topic_received : m_topics_subscribed ) all_topics_received &= topic_received.second;
        return all_topics_received;
    }
    
    bool topicsReceived( ) const 
    {
        bool all_topics_received = true;
        for( const std::pair<std::string,bool>& topic_received : m_topics_subscribed ) all_topics_received &= topic_received.second;
        return all_topics_received;
    }
    void resetTopicsReceived( ) 
    {
      for( auto & topic_received : m_topics_subscribed ) topic_received.second = false;
    }
    
  };
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
inline 
JointClaimedResource::JointClaimedResource( const itia_hardware_interface::JointResource& jr, ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received  )
: itia_hardware_interface::ClaimedResource<sensor_msgs::JointState> ( jr, jr.m_joint_names, robot_hw_nh, topics_received )
{
  m_nAx  = m_resource_names .size();
  m_pos.resize(m_nAx, 0);
  m_vel.resize(m_nAx, 0);
  m_eff.resize(m_nAx, 0);
  
  m_cmd_pos.resize(m_nAx, 0);
  m_cmd_vel.resize(m_nAx, 0);
  m_cmd_eff.resize(m_nAx, 0);
  
  m_cmd_pos=m_pos;
  m_cmd_vel=m_vel;
  m_cmd_pos=m_eff;
    
}

inline 
void JointClaimedResource::callback(const sensor_msgs::JointStateConstPtr& msg, const std::string& topic )
{
  ClaimedResource::callback( msg, topic );
  
  std::lock_guard<std::mutex> lock(m_mutex);
  std::vector<std::string> names=msg->name;
  std::vector<double> pos=msg->position;
  std::vector<double> vel=msg->velocity;
  std::vector<double> eff=msg->effort;
  
  std::vector<unsigned int> ax_indexes = m_idxes_ax_map[ topic ];
  if ( (pos.size()<ax_indexes.size()) || (vel.size()<ax_indexes.size()) || (eff.size()<ax_indexes.size()) || (names.size()<ax_indexes.size()))
  {
    ROS_FATAL("Dimension are wrong: pos %zu, vel %zu, eff %zu, names %zu, expected %zu",pos.size(),vel.size(),eff.size(),names.size(),ax_indexes.size());
    m_topics_received[topic]=false;
    return;
  }
  
  if (!name_sorting::permutationName(m_resource_names_map.at(topic),names,pos,vel,eff))
  {
    m_topics_received[topic]=false;
    ROS_WARN_THROTTLE(0.1,"[%s] feedback joint states names are wrong!", m_console.c_str());
    return;
  }
  m_topics_received[topic]=true;
  for (unsigned int idx=0;idx<m_resource_names_map.at(topic).size();idx++)
  {
    m_pos.at(ax_indexes.at(idx))=pos.at(idx);
    m_vel.at(ax_indexes.at(idx))=vel.at(idx);
    m_eff.at(ax_indexes.at(idx))=eff.at(idx);
  }
}

inline 
void JointClaimedResource::write(const ros::Time& time, const ros::Duration& period)
{
  if( m_pub )
  {
    if (!m_p_jh_active && !m_v_jh_active && !m_e_jh_active)
    {
      return;
    }
    
    if (m_p_jh_active)
      m_msg->position = m_cmd_pos;
    else
    {
      m_msg->position.resize(m_nAx);
      std::fill(m_msg->position.begin(),m_msg->position.end(),0.0);
    }
    
    if (m_v_jh_active)
      m_msg->velocity = m_cmd_vel;
    else
    {
      m_msg->velocity.resize(m_nAx);
      std::fill(m_msg->velocity.begin(),m_msg->velocity.end(),0.0);
    }
    
    if (m_e_jh_active)
      m_msg->effort   = m_cmd_eff;
    else
    {
      m_msg->effort.resize(m_nAx);
      std::fill(m_msg->effort.begin(),m_msg->effort.end(),0.0);
    }
    m_msg->name=m_resource_names;
    m_msg->header.stamp=ros::Time::now();
    
    ClaimedResource::write( time, period );
  }
}

inline 
bool JointClaimedResource::prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
  bool p_jh_active, v_jh_active, e_jh_active;
  p_jh_active=v_jh_active=e_jh_active=false;
  
  for (const hardware_interface::ControllerInfo& controller: start_list)
  {
    
    for (const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      if (!res.hardware_interface.compare("hardware_interface::PositionJointInterface"))
        p_jh_active=true;
      if (!res.hardware_interface.compare("hardware_interface::VelocityJointInterface"))
        v_jh_active=true;
      if (!res.hardware_interface.compare("hardware_interface::EffortJointInterface"))
        e_jh_active=true;
      if (!res.hardware_interface.compare("hardware_interface::VelEffJointInterface"))
      {
        v_jh_active=true;
        e_jh_active=true;
      }
      if (!res.hardware_interface.compare("hardware_interface::PosVelEffJointInterface"))
      {
        p_jh_active=true;
        v_jh_active=true;
        e_jh_active=true;
      }
    }
  }
  
  m_p_jh_active=p_jh_active;
  m_v_jh_active=v_jh_active;
  m_e_jh_active=e_jh_active;
  return true;
}


/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
inline 
AnalogClaimedResource::AnalogClaimedResource( const itia_hardware_interface::AnalogResource& ar, ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received )
: itia_hardware_interface::ClaimedResource<std_msgs::Float64MultiArray> ( ar, ar.m_channel_names, robot_hw_nh, topics_received )
{
  m_state.resize( ar.m_num_channels, 0 );
  m_output.resize( ar.m_num_channels, 0 );

}
inline 
void AnalogClaimedResource::callback(const std_msgs::Float64MultiArray::ConstPtr& msg, const std::string& topic )
{
  ClaimedResource::callback( msg, topic );
  
  std::lock_guard<std::mutex> lock(m_mutex);
  if ( msg->data.size() != m_state.size() )
  {
    ROS_FATAL("Dimension are wrong: msg %zu, state %zu", msg->data.size(),m_state.size() );
    m_topics_received[topic]=false;
    return;
  }
  std::vector<std::string> names( m_state.size( ) );
  for( size_t i=0; i<m_state.size( ); i++  ) 
  {
    names[i] = msg->layout.dim[i].label;
  }
  
  std::vector<double>       values = msg->data;
  std::vector<unsigned int> ax_indexes = m_idxes_ax_map[ topic ];
  if ( values.size()<ax_indexes.size() )
  {
    ROS_FATAL("Dimension are wrong: val %zu, expected %zu", values.size(), ax_indexes.size());
    m_topics_received[topic]=false;
    return;
  }
  
  if (!name_sorting::permutationName(m_resource_names_map.at(topic),names,values))
  {
    m_topics_received[topic]=false;
    ROS_WARN_THROTTLE(0.1,"[%s] feedback joint states names are wrong!", m_console.c_str());
    return;
  }
  m_topics_received[topic]=true;
  for (unsigned int idx = 0; idx < m_resource_names_map.at( topic ).size();idx++)
  {
    m_state.at( ax_indexes.at(idx) ) = values.at(idx);
  }
}

inline 
void AnalogClaimedResource::write(const ros::Time& time, const ros::Duration& period)
{
  if( m_pub )
  {
    m_msg->data = m_output; 
    
    ClaimedResource::write( time, period );
  }
}

inline 
bool AnalogClaimedResource::prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
  bool a_h_active = m_a_h_active;
  bool a_sh_active = m_a_sh_active;
  for (const hardware_interface::ControllerInfo& controller: start_list)
  {
    for (const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      a_sh_active = !res.hardware_interface.compare("hardware_interface::AnalogStateInterface");
      a_h_active = !res.hardware_interface.compare("hardware_interface::AnalogInterface");
    }
  }
  m_a_h_active = a_h_active;
  m_a_sh_active = a_sh_active;
  return true;
}


/**
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
inline 
ForceTorqueClaimedResource::ForceTorqueClaimedResource( const itia_hardware_interface::ForceTorqueResource& wr, ros::NodeHandle& robot_hw_nh, std::map< std::string, bool> & topics_received )
: itia_hardware_interface::ClaimedResource< geometry_msgs::WrenchStamped > ( wr, {wr.m_sensor_name}, robot_hw_nh, topics_received )
{
  size_t l = __LINE__;
  try
  {
    l = __LINE__;
    m_frame_id = wr.m_frame_id;
    m_state.resize( 6, 0 );
    l = __LINE__;
    m_output.resize( 6, 0 );
    l = __LINE__;
  }
  catch(std::exception& e)
  {
    ROS_FATAL("ClaimedResource error %s (at line:%zu)", e.what(), l);
    std::cout.flush();
    fflush(stdout); 
    fflush(stderr); 
    throw std::runtime_error( ("ClaimedResource error"  + std::string( e.what() ) ) .c_str() );
  }
  catch(...)
  {
    ROS_FATAL("ClaimedResource error. Unhandled expcetion at line:%zu",l);
    std::cout.flush();
    fflush(stdout); 
    fflush(stderr); 
    throw std::runtime_error( "ClaimedResource error"  );
  }


}

inline 
void ForceTorqueClaimedResource::callback(const geometry_msgs::WrenchStamped::ConstPtr& msg, const std::string& topic )
{
  ClaimedResource::callback( msg, topic );
  
  std::lock_guard<std::mutex> lock(m_mutex);
  
  m_topics_received[topic] = true;
  m_state.at(0) = msg->wrench.force.x;
  m_state.at(1) = msg->wrench.force.y;
  m_state.at(2) = msg->wrench.force.z;
  m_state.at(3) = msg->wrench.torque.x;
  m_state.at(4) = msg->wrench.torque.y;
  m_state.at(5) = msg->wrench.torque.z;
}

inline 
void ForceTorqueClaimedResource::write(const ros::Time& time, const ros::Duration& period)
{
  if( m_pub )
  {
    m_msg->wrench.force.x  = m_output.at(0);
    m_msg->wrench.force.y  = m_output.at(1);
    m_msg->wrench.force.z  = m_output.at(2);
    m_msg->wrench.torque.x = m_output.at(3);
    m_msg->wrench.torque.y = m_output.at(4);
    m_msg->wrench.torque.z = m_output.at(5);
      
    ClaimedResource::write( time, period );
  }
}

inline 
bool ForceTorqueClaimedResource::prepareSwitch(const std::list< hardware_interface::ControllerInfo >& start_list, const std::list< hardware_interface::ControllerInfo >& stop_list)
{
  bool h_active = m_w_h_active;
  bool sh_active = m_w_sh_active;
  for (const hardware_interface::ControllerInfo& controller: start_list)
  {
    for (const hardware_interface::InterfaceResources& res: controller.claimed_resources)
    {
      sh_active = !res.hardware_interface.compare("hardware_interface::ForceTorqueStateHandle");
      h_active  = !res.hardware_interface.compare("hardware_interface::ForceTorqueInterface");
    }
  }
  m_w_h_active = h_active;
  m_w_sh_active = sh_active;
  return true;
}

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}

#endif
